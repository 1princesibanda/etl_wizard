/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.datatrain.ui;

import com.gew.datatrain.Controller.mysqlcontroller.IApplication;
import com.gew.datatrain.Controller.mysqlcontroller.IControllee;
import com.gew.datatrain.Controller.mysqlcontroller.IView;
import java.util.Hashtable;
import java.util.Random;
import java.util.Set;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author 0400626
 */
public abstract class IDataTrainView extends Application implements IView
{

	private static Hashtable<String, Boolean> map = new Hashtable<>();
	final private static Random random = new Random();
	private static String unique = "";
	protected IDataTrainView nextView;
	protected IDataTrainView previousView;
	protected Stage stage;
	protected Set<IApplication> applicationsSet;

	//abstract public void start(Stage stage);
//We trust that the random.nextDouble() can produce more than enough numbers to uniquely allocate ids without running out/duplicates.
	//If this lets us down, we'll trust that the machine has enough memory for us to construct a sufficiently long string to uniquely id objects.
	public static String uniqueId()
	{
		unique = "" + random.nextDouble();
		Boolean used = map.put(unique, true);

		while (used != null)
		{
			unique += unique;
			used = map.put(unique, true);
		}
		return unique;
	}

	public void setNextView(IDataTrainView nextView)
	{
		this.nextView = nextView;
	}

	public IDataTrainView getNextView()
	{
		return this.nextView;
	}

	public void setPreviousView(IDataTrainView previousView)
	{
		this.previousView = previousView;
	}

	public IDataTrainView getPreviousView()
	{
		return this.previousView;
	}

	public void setStage(Stage stage)
	{
		this.stage = stage;
		if (this.stage == null)
			System.out.println("cView3: setStage(): Warning: object stage set to null!");
	}

	//public boolean equals()
}
