/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.datatrain.ui;

import com.gew.datatrain.Controller.mysqlcontroller.IController;
import com.gew.datatrain.Controller.mysqlcontroller.cMySQLController;
import com.gew.datatrain.ui.cView3.Properties2;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author 0400626
 */
public final class cView4 extends IDataTrainView implements EventHandler<Event>
{

	Pane pane;
	Scene scene;
	IController controller;
	Bounds bounds;
	Text scenetitle;

	Label propertiesLabel, transformationsLabel, transformationsComboBoxLabel, mapNamesLabel, mapNameTextAreaLabel;
	TableColumn columnNameColumn1, columnNameColumn2, fileReferenceColumn, typeColumn, tranformationColumn;
	Button generateFileButton, saveMapButton, addTransformationButton, removeTransformationButton, backButton;
	ComboBox transformationsComboBox;
	TableView<cView3.Properties2> propertiesTable;
	TableView<Properties> transformationsTable;
	TextInputDialog mapNameDialog, mapfilenameDialog;

	private static String DEFAULT_PROPERTIES_TABLE_NAME = "tblProperties", DEFAULT_TRANSFORMATIONS_TABLE_NAME = "tblTransformations", DEFAULT_MAPS_FILENAME_TABLE_NAME = "tblMapsFilenames", DEFAULT_MAP_NAME_TABLE_NAME = "tblMaps", DEFAULT_MAPS_TABLE_MAP_NAME_COLUMN_NAME = "mapName";
	private static final List<String> DEFAULT_PROPERTIES_TABLE_COLUMN_NAMES = Arrays.asList("propertyId", "columnName", "fileReference", "type", "mapName"), DEFAULT_TRANSFORMATIONS_TABLE_COLUMN_NAMES = Arrays.asList("transformationId", "columnName", "transformation"), DEFAULT_MAPS_FILENAME_TABLE_COLUMN_NAMES = Arrays.asList("filenameId", "filename");

	private List<String> propertiesTableColumnNames, transformationsTableColumnNames, mapFilenamesTableColumnNames;

	private String propertiesTableName, transformationsTableName, mapsFilenameTableName, mapsTableName, mapsTableMapNameColumnName;

	private boolean INITIALISED = false;

	ObservableList<Properties> transformationsTableDataList;
	ObservableList<cView3.Properties2> propertiesTableDataList;
	ObservableList<String> transformationsComboBoxList, mapNamesList;
	List<String> transformationsComboBoxInitList;
	ListView<String> mapsListView;

	TextArea mapNameTextArea;

	PropertyValueFactory<Properties2, String> columnNamePropertyFactory, fileReferencePropertyFactory, typePropertyFactory;
	PropertyValueFactory<Properties, String> columnNameColumn2Factory, transformationColumnFactory;

	Task<Void> task;
	Thread thread;

	public cView4(IController controller)
	{
		this();
		this.controller = controller;
	}

	public cView4(IController controller, IDataTrainView previousView, IDataTrainView nextView)
	{
		this();
		this.controller = controller;
		this.previousView = previousView;
		super.nextView = nextView;
	}

	public cView4(IController controller, String propertiesTable)
	{
		this();
		this.controller = controller;
		this.propertiesTableName = propertiesTable;
	}

	public cView4()
	{
		this.pane = new Pane();
		this.scene = new Scene(this.pane, 1300, 500);
		this.stage = null;

		this.scenetitle = new Text("Transformations");

		this.mapNamesLabel = new Label("MAP NAMES");
		this.propertiesLabel = new Label("PROPERTIES");
		this.transformationsComboBoxLabel = new Label("Transformation");
		this.transformationsLabel = new Label("TRANSFORMATIONS");
		this.mapNameTextAreaLabel = new Label("Enter New Map Name:");

		this.propertiesTable = new TableView<>();
		this.transformationsTable = new TableView<>();

		this.mapsListView = new ListView<>();

		this.backButton = new Button("Back");
		this.generateFileButton = new Button("GenerateFile");
		this.saveMapButton = new Button("Save Map");
		this.addTransformationButton = new Button("addTransformation");
		this.removeTransformationButton = new Button("removeTransformation");
		this.mapNameDialog = new TextInputDialog("Map1"); //init with this.mapNameDialog.setContentText("Map1"); 
		this.mapfilenameDialog = new TextInputDialog("out.txt");

		this.transformationsComboBox = new ComboBox();

		this.columnNameColumn1 = new TableColumn("Column Name");
		this.fileReferenceColumn = new TableColumn("Is File Reference");
		this.typeColumn = new TableColumn("Type");

		this.propertiesTableDataList = FXCollections.observableArrayList();
		this.transformationsTableDataList = FXCollections.observableArrayList();
		this.transformationsComboBoxList = FXCollections.observableArrayList();
		this.mapNamesList = FXCollections.observableArrayList();

		this.columnNameColumn2 = new TableColumn("Column Name");
		this.tranformationColumn = new TableColumn("Transformation");

		this.columnNamePropertyFactory = new PropertyValueFactory<>("ColumnName");
		this.fileReferencePropertyFactory = new PropertyValueFactory<>("FileReference");
		this.typePropertyFactory = new PropertyValueFactory<>("Type");

		this.columnNameColumn2Factory = new PropertyValueFactory<>("ColumnName");;
		this.transformationColumnFactory = new PropertyValueFactory<>("Transformation");

		this.mapNameTextArea = new TextArea();

		this.propertiesTableName = null;

		this.transformationsTableName = null;

		this.propertiesTableColumnNames = null;

		this.transformationsTableColumnNames = null;

		this.mapFilenamesTableColumnNames = null;

		this.mapsFilenameTableName = null;

		this.mapsTableName = null;

		this.mapsTableMapNameColumnName = null;

		this.controller = null;
//
		this.task = new Task<Void>()
		{
			@Override
			protected Void call() throws Exception
			{
				while (!isCancelled())
				{
					if ((mapNamesList.contains(mapNameTextArea.getText())) || (mapNameTextArea.getText().equals("")))
					//if ( (mapNameTextArea.getText().equals("")))
					{
						mapsListView.setDisable(true);
						propertiesLabel.setDisable(true);
						transformationsComboBoxLabel.setDisable(true);
						transformationsLabel.setDisable(true);
						propertiesTable.setDisable(true);
						transformationsTable.setDisable(true);
						transformationsComboBox.setDisable(true);
						mapNamesLabel.setDisable(true);
					}
					else
					{
						mapsListView.setDisable(false);
						propertiesLabel.setDisable(false);
						transformationsComboBoxLabel.setDisable(false);
						transformationsLabel.setDisable(false);
						propertiesTable.setDisable(false);
						transformationsTable.setDisable(false);
						transformationsComboBox.setDisable(false);
						mapNamesLabel.setDisable(false);
					}
					mapNameTextAreaLabel.setDisable(false);
					mapNameTextArea.setDisable(false); //this widget always active
					Thread.sleep(100); //arguments are milliseconds 
				}
				return null;
			}
		};

		super.nextView = null;
		this.previousView = null;

		this.init();
	}

	@Override
	public void init()
	{

		this.scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 20));
		this.scenetitle.setId(IDataTrainView.uniqueId());
		this.scenetitle.setX(700);
		this.scenetitle.setY(50);

		bounds = this.mapNamesLabel.getLayoutBounds();
		this.mapNamesLabel.setDisable(true);
		Region.positionInArea(this.mapNamesLabel, 100, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNamesLabel.setId(IDataTrainView.uniqueId());

		bounds = this.mapNameTextAreaLabel.getLayoutBounds();
		this.mapNameTextAreaLabel.setDisable(false);
		Region.positionInArea(this.mapNameTextAreaLabel, 100, 80, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNameTextAreaLabel.setId(IDataTrainView.uniqueId());

		bounds = this.propertiesLabel.getLayoutBounds();
		Region.positionInArea(this.propertiesLabel, 400, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.propertiesLabel.setId(IDataTrainView.uniqueId());

		this.transformationsLabel.getLayoutBounds();
		Region.positionInArea(this.transformationsLabel, 950, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.transformationsLabel.setId(IDataTrainView.uniqueId());

		bounds = this.transformationsComboBoxLabel.getLayoutBounds();
		Region.positionInArea(this.transformationsComboBoxLabel, 770, 225, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.transformationsComboBoxLabel.setId(IDataTrainView.uniqueId());

		this.mapsListView.setItems(this.mapNamesList);
		this.mapsListView.setDisable(true);
		this.bounds = this.mapsListView.getLayoutBounds();
		this.mapsListView.setPrefSize(240, 300);
		this.mapsListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.mapsListView, 100, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapsListView.setDisable(true);
		this.mapsListView.setOnMouseClicked(this);

		this.bounds = this.mapNameTextArea.getLayoutBounds();
		Region.positionInArea(this.mapNameTextArea, 240, 70, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNameTextArea.setPrefSize(200, 10);
		this.mapNameTextArea.setId(IDataTrainView.uniqueId());
		this.mapNameTextArea.setOnKeyTyped(this);
		this.mapNameTextArea.setDisable(false);
		this.mapNameTextArea.setPromptText("Enter map name here.");

		//TableColumn columnNameColumn, fileReferenceColumn, typeColumn;
		this.columnNameColumn1.setResizable(true);
		this.columnNameColumn1.setPrefWidth(150);
		this.columnNameColumn1.setId(IDataTrainView.uniqueId());
		this.columnNameColumn1.setCellValueFactory(this.columnNamePropertyFactory);
		this.columnNameColumn2.setResizable(true);
		this.columnNameColumn2.setPrefWidth(100);
		this.columnNameColumn2.setId(IDataTrainView.uniqueId());
		this.columnNameColumn2.setCellValueFactory(this.columnNameColumn2Factory);
		this.fileReferenceColumn.setResizable(true);
		this.fileReferenceColumn.setPrefWidth(100);
		this.fileReferenceColumn.setId(IDataTrainView.uniqueId());
		this.fileReferenceColumn.setCellValueFactory(this.fileReferencePropertyFactory);
		this.typeColumn.setResizable(true);
		this.typeColumn.setPrefWidth(50);
		this.typeColumn.setId(IDataTrainView.uniqueId());
		this.typeColumn.setCellValueFactory(this.typePropertyFactory);
		this.tranformationColumn.setResizable(true);
		this.tranformationColumn.setPrefWidth(200);
		this.tranformationColumn.setId(IDataTrainView.uniqueId());
		this.tranformationColumn.setCellValueFactory(this.transformationColumnFactory);

		//    TableView<cView3.Properties2> propertiesTable;
		bounds = this.propertiesTable.getLayoutBounds();
		this.propertiesTable.setPrefSize(300, 300);
		Region.positionInArea(this.propertiesTable, 400, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.propertiesTable.setOnMouseClicked(this);
		this.propertiesTable.setId(IDataTrainView.uniqueId());
		this.propertiesTable.setEditable(false);
		this.propertiesTable.setVisible(true);
		this.propertiesTable.setItems(this.propertiesTableDataList);
		this.propertiesTable.getColumns().setAll(this.columnNameColumn1, this.fileReferenceColumn, this.typeColumn);

		bounds = this.transformationsTable.getLayoutBounds();
		this.transformationsTable.setPrefSize(300, 300);
		Region.positionInArea(this.transformationsTable, 950, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.transformationsTable.setOnMouseClicked(this);
		this.transformationsTable.setId(IDataTrainView.uniqueId());
		this.transformationsTable.setEditable(false);
		this.transformationsTable.setVisible(true);
		this.transformationsTable.setItems(this.transformationsTableDataList);
		this.transformationsTable.getColumns().setAll(this.columnNameColumn2, this.tranformationColumn);

		this.backButton.setDisable(false);
		this.backButton.setId(IDataTrainView.uniqueId());
		this.generateFileButton.setDisable(true);
		this.generateFileButton.setId(IDataTrainView.uniqueId());
		this.saveMapButton.setDisable(true);
		this.saveMapButton.setId(IDataTrainView.uniqueId());
		this.addTransformationButton.setDisable(true);
		this.addTransformationButton.setId(IDataTrainView.uniqueId());
		this.removeTransformationButton.setDisable(true);
		this.removeTransformationButton.setId(IDataTrainView.uniqueId());

		bounds = this.backButton.getLayoutBounds();
		Region.positionInArea(this.backButton, 1300, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.backButton.setOnMouseClicked(this);

		bounds = this.generateFileButton.getLayoutBounds();
		Region.positionInArea(this.generateFileButton, 1150, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.generateFileButton.setOnMouseClicked(this);
		this.generateFileButton.setId(IDataTrainView.uniqueId());

		bounds = this.saveMapButton.getLayoutBounds();
		Region.positionInArea(this.saveMapButton, 1035, 470, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.saveMapButton.setOnMouseClicked(this);

		bounds = this.transformationsComboBox.getLayoutBounds();
		this.transformationsComboBox.setPrefSize(140, 10);
		Region.positionInArea(this.transformationsComboBox, 750, 245, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.transformationsComboBox.setOnMouseClicked(this);
		this.transformationsComboBox.setId(IDataTrainView.uniqueId());
		this.transformationsComboBox.setItems(this.transformationsComboBoxList);

		bounds = this.addTransformationButton.getLayoutBounds();
		Region.positionInArea(this.addTransformationButton, 750, 295, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.addTransformationButton.setOnMouseClicked(this);

		bounds = this.removeTransformationButton.getLayoutBounds();
		Region.positionInArea(this.removeTransformationButton, 750, 340, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.removeTransformationButton.setOnMouseClicked(this);

		this.transformationsComboBoxInitList = new LinkedList<>();
		this.transformationsComboBoxInitList.add("STRING_TO_INT");
		this.transformationsComboBoxInitList.add("STRING_TO_LONG");
		this.transformationsComboBoxInitList.add("STRING_TO_DOUBLE");
		this.transformationsComboBoxInitList.add("STRING_TO_FLOAT");
		this.transformationsComboBoxInitList.add("STRING_TO_DATE");
		this.transformationsComboBoxInitList.add("DATE_TO_LONG");
		this.transformationsComboBoxInitList.add("KILOMETER_TO_METER");
		this.transformationsComboBoxInitList.add("METER_TO_CENTIMETER");
		this.transformationsComboBoxInitList.add("METER_TO_MILIMETER");
		this.transformationsComboBoxInitList.add("METER_TO_KILOMETER");
		this.transformationsComboBoxInitList.add("CENTIMETER_TO_METER");
		this.transformationsComboBoxInitList.add("MILIMETER_TO_METER");
		this.transformationsComboBoxInitList.add("MILE_TO_YARDS");
		this.transformationsComboBoxInitList.add("YARDS_TO_FEET");
		this.transformationsComboBoxInitList.add("FEET_TO_INCH");
		this.transformationsComboBoxInitList.add("CHAIN_TO_YARDS");
		this.transformationsComboBoxInitList.add("FURLONG_TO_CHAIN");
		this.transformationsComboBoxInitList.add("MILE_TO_FURLONG");
		this.transformationsComboBoxInitList.add("LEAGUE_TO_MILE");
		this.transformationsComboBoxInitList.add("FATHOM_TO_YARDS");
		this.transformationsComboBoxInitList.add("CABLE_TO_FATHOM");
		this.transformationsComboBoxInitList.add("NAUTICALMILE_TO_CABLE");
		this.transformationsComboBoxInitList.add("YARDS_TO_MILE");
		this.transformationsComboBoxInitList.add("FEET_TO_YARDS");
		this.transformationsComboBoxInitList.add("INCH_TO_FEET");
		this.transformationsComboBoxInitList.add("YARDS_TO_CHAIN");
		this.transformationsComboBoxInitList.add("CHAIN_TO_FURLONG");
		this.transformationsComboBoxInitList.add("FURLONG_TO_MILE");
		this.transformationsComboBoxInitList.add("MILE_TO_LEAGUE");
		this.transformationsComboBoxInitList.add("YARDS_TO_FATHOM");
		this.transformationsComboBoxInitList.add("FATHOM_TO_CABLE");
		this.transformationsComboBoxInitList.add("CABLE_TO_NAUTICALMILE");
		this.transformationsComboBoxInitList.add("KILOMETER_TO_MILE");
		this.transformationsComboBoxInitList.add("MILE_TO_KILOMETER");
		this.transformationsComboBoxInitList.add("KILOGRAM_TO_POUND");
		this.transformationsComboBoxInitList.add("KILOGRAM_TO_TON");
		this.transformationsComboBoxInitList.add("KILOGRAM_TO_GRAM");
		this.transformationsComboBoxInitList.add("POUND_TO_KILOGRAM");
		this.transformationsComboBoxInitList.add("POUND_TO_TON");
		this.transformationsComboBoxInitList.add("GRAM_TO_KILOGRAM");
		this.transformationsComboBoxInitList.add("FILEURL_TO_TEXT");
		this.transformationsComboBoxList.setAll(this.transformationsComboBoxInitList);

		this.mapNameDialog.setTitle("New Map Creation");
		this.mapNameDialog.setHeaderText("Your new Map Needs a name");
		this.mapNameDialog.setContentText("Please enter name for your new Map:");

		this.mapfilenameDialog.setTitle("New File Creation");
		this.mapfilenameDialog.setHeaderText("Your new File Needs a name");
		this.mapfilenameDialog.setContentText("Please enter file name for your new file:");

		this.propertiesTableName = this.propertiesTableName == null ? cView4.DEFAULT_PROPERTIES_TABLE_NAME : this.propertiesTableName;
		this.transformationsTableName = this.transformationsTableName == null ? cView4.DEFAULT_TRANSFORMATIONS_TABLE_NAME : this.transformationsTableName;
		this.propertiesTableColumnNames = this.propertiesTableColumnNames == null ? cView4.DEFAULT_PROPERTIES_TABLE_COLUMN_NAMES : this.propertiesTableColumnNames;
		this.transformationsTableColumnNames = this.transformationsTableColumnNames == null ? cView4.DEFAULT_TRANSFORMATIONS_TABLE_COLUMN_NAMES : this.transformationsTableColumnNames;
		this.mapsFilenameTableName = this.mapsFilenameTableName == null ? cView4.DEFAULT_MAPS_FILENAME_TABLE_NAME : this.mapsFilenameTableName;
		this.mapFilenamesTableColumnNames = this.mapFilenamesTableColumnNames == null ? cView4.DEFAULT_MAPS_FILENAME_TABLE_COLUMN_NAMES : this.mapFilenamesTableColumnNames;
		this.mapsTableName = this.mapsTableName == null ? cView4.DEFAULT_MAP_NAME_TABLE_NAME : this.mapsTableName;
		this.mapsTableMapNameColumnName = this.mapsTableMapNameColumnName == null ? cView4.DEFAULT_MAPS_TABLE_MAP_NAME_COLUMN_NAME : this.mapsTableMapNameColumnName;

		this.INITIALISED = true;

		this.thread = null;
	}

	//make new scene using the pane
	@Override
	public void start(Stage stage)
	{
		if (!this.INITIALISED)
			this.init();
		assert this.INITIALISED;

		//configure the stage
		stage.setTitle("Text Data Format Converter");

		stage.setMaxHeight(550);
		stage.setMaxWidth(1600);
		stage.setMinHeight(600);
		stage.setMinWidth(1600);
		stage.setFullScreen(false);
		stage.setResizable(false); //Toggles availability of Windows minimise and maximise. if set to false and setMaximized() is set, then it is fixed to the setting of setMaximized()
		stage.setMaximized(false);
		//stage.toFront(); Not really data- these are meant for use at runtime
		stage.setIconified(false); //Puts the window down at the bottom task bar where the open programs are. Takes precedence over toBack() and toFront();
		stage.setFullScreenExitHint("This is exit hint");
		stage.setAlwaysOnTop(false);

		this.stage = stage;
		this.pane.getChildren().setAll(this.scenetitle, this.mapNamesLabel, this.mapNameTextAreaLabel, this.propertiesLabel, this.mapNameTextArea, this.transformationsLabel, this.transformationsComboBoxLabel, this.mapsListView, this.generateFileButton, this.saveMapButton, this.addTransformationButton, this.removeTransformationButton, this.backButton, this.transformationsComboBox, this.propertiesTable, this.transformationsTable);

		stage.setScene(this.scene);
		stage.show();//remove when take out main method
		this.thread = new Thread(this.task);
		this.thread.start();
	}

	@Override
	public void handle(Event e)
	{
		Node source = (Node) e.getSource(); //Should always be a Node on the Scene graph
		try
		{
			if (source.getId().equals(this.backButton.getId()))
			{
				this.task.cancel();

				try
				{
					if (this.thread != null)
					{
						this.thread.join();
					}
				}
				catch (Exception ex)
				{
					System.out.println(ex);
				}

				if (this.previousView != null)
				{
					this.pane.getChildren().clear();
					//this.previousView.start(this.stage);
					String outputPropertiesTableName = "tblPropertiesInput";
					List<String> outValuesList, outColumnsList, outTableNameList;
					List<List<String>> outValuesListList, outColumnsListList;
					Iterator<Properties2> propertiesIter;
					Properties2 prop;

					//Go through each of the properties and write them out to a table
					outValuesListList = new LinkedList<>();
					outColumnsListList = new LinkedList<>();
					outTableNameList = new LinkedList<>();
					propertiesIter = this.propertiesTableDataList.iterator();

					while (propertiesIter.hasNext())
					{
						prop = propertiesIter.next();
						outValuesList = new LinkedList<>();
						outValuesList.add(prop.getColumnName());
						outValuesList.add(prop.getFileReference());
						outValuesList.add(prop.getType());
						outValuesList.add(this.mapNameTextArea.getText());
						outColumnsList = new LinkedList<>(this.propertiesTableColumnNames);
						outColumnsList.remove(0);
						outColumnsListList.add(outColumnsList);
						outTableNameList.add(outputPropertiesTableName);
						outValuesListList.add(outValuesList);
						
					}

					this.controller.write(this, outTableNameList, outColumnsListList, outValuesListList);
					System.out.println("view4 starting previous view...");
				}

			}
			else if (source.getId().equals(this.addTransformationButton.getId()))
			{
				int selectedPropertyIndex = this.propertiesTable.getSelectionModel().getSelectedIndex();
				int selectedTransformationIndex = this.transformationsComboBox.getSelectionModel().getSelectedIndex();
				String columnName, transformationChoice;
				Properties transformation;
				if ((selectedPropertyIndex >= 0) && (selectedTransformationIndex >= 0))
				{
					Properties2 prop = this.propertiesTableDataList.get(selectedPropertyIndex);
					columnName = prop.getColumnName();
					transformationChoice = this.transformationsComboBoxList.get(selectedTransformationIndex);
					transformation = new Properties(columnName, transformationChoice);
					this.transformationsTableDataList.add(transformation);
					this.addTransformationButton.setDisable(true);
					if (this.transformationsTableDataList.size() == this.propertiesTableDataList.size())
						this.saveMapButton.setDisable(false);
				}
				else
				{
					assert false : "Execution should not be allowed to reach here- use button enable/disable to prevent clicking of addTransformationButton before there is sensible data selected.";
					System.out.println("cView4: Index outside data: " + selectedPropertyIndex);
				}
			}
			else if (source.getId().equals(this.removeTransformationButton.getId()))
			{
				int selectedPropertyIndex = this.transformationsTable.getSelectionModel().getSelectedIndex();
				if (selectedPropertyIndex >= 0)
				{
					this.removeTransformationButton.setDisable(true);
					this.addTransformationButton.setDisable(true);
					Properties prop = this.transformationsTableDataList.get(selectedPropertyIndex);
					this.transformationsTableDataList.remove(prop);

					this.saveMapButton.setDisable(true);
					assert this.transformationsTableDataList.size() != this.propertiesTableDataList.size() : "removing a transformation means there isnt a transformation for each of the properties, so dont allow user to create map with missing transformations.";
				}
				else
				{
					this.addTransformationButton.setDisable(true);
					this.removeTransformationButton.setDisable(true);
					System.out.println("cView4: Index outside data: " + selectedPropertyIndex);
				}
			}
			/*
			else if (source.getId().equals(nextButton.getId()))
			{
				this.task.cancel();
				//this.thread = new Thread(task);
				try
				{
					if (this.thread != null)
					{
						this.thread.join();
					}
				}
				catch (Exception ex)
				{
					System.out.println(ex);
				}

				if (super.nextView == null)
				{
					super.nextView = new cView3(this.controller, this, null);
				}
				this.pane.getChildren().clear();

				super.nextView.start(this.stage);
			}
			 */
			else if (source.getId().equals(this.propertiesTable.getId()))
			{
				int selectedPropertyIndex = this.propertiesTable.getSelectionModel().getSelectedIndex();
				int selectedTransformationIndex = this.transformationsComboBox.getSelectionModel().getSelectedIndex();
				if ((selectedPropertyIndex >= 0) && (selectedTransformationIndex >= 0))
				{
					Properties2 prop = this.propertiesTableDataList.get(selectedPropertyIndex);
					//enable addTransformationButton only when the selected property isnt with a transformation already
					String columnName = prop.getColumnName();
					Iterator<Properties> transIter = transformationsTableDataList.iterator();
					this.addTransformationButton.setDisable(false);
					Properties trans = null;

					if (transIter.hasNext())
					{
						while (transIter.hasNext())
						{
							trans = transIter.next();
							if (trans.getColumnName().equals(columnName))
							{
								this.addTransformationButton.setDisable(true);
								break;
							}
						}
					}
					else
						this.addTransformationButton.setDisable(false);

					this.removeTransformationButton.setDisable(true);
					//this.propertiesTableDataList.remove(prop);
					//just set the index and then addButton will use it to add to transformations
				}
				else
				{
					this.addTransformationButton.setDisable(true);
					this.removeTransformationButton.setDisable(true);
					System.out.println("cView4: Index outside data: " + selectedPropertyIndex);
				}

			}
			else if (source.getId().equals(this.saveMapButton.getId()))
			{
				//writeTransformations();

				writeMap();
				this.saveMapButton.setDisable(false);
				this.generateFileButton.setDisable(false);
				this.addTransformationButton.setDisable(true);
				this.removeTransformationButton.setDisable(true);
				//this.transformationsComboBox.disarm();
			}
			else if (source.getId().equals(this.generateFileButton.getId()))
			{
				Iterator<Properties> iter = this.transformationsTableDataList.iterator();
				List<String> outFileNamesList;

				LinkedList<String> tableColumnsList = new LinkedList<>(this.mapFilenamesTableColumnNames);
				tableColumnsList.removeFirst();
				outFileNamesList = new LinkedList<>();

				Optional<String> result = this.mapfilenameDialog.showAndWait();

				if (result.isPresent())//use the name when you write to controller- do it like we do table name and populate for every row
				{
					outFileNamesList.add(result.get());
					this.controller.write(this, this.mapsFilenameTableName, tableColumnsList, outFileNamesList);

					this.saveMapButton.setDisable(true);
					this.generateFileButton.setDisable(true);
					this.addTransformationButton.setDisable(true);
					this.removeTransformationButton.setDisable(true);
					this.transformationsComboBox.disarm();
				}
				else
				{
					System.out.println("New Map not created because no name was provided.");
					Alert alert = new Alert(Alert.AlertType.WARNING);
					alert.setTitle("Warning");
					alert.setHeaderText("Problem creating your new Map");
					alert.setContentText("New Map not created because no name was provided.");
					alert.showAndWait();
				}
			}
			else if (source.getId().equals(this.transformationsTable.getId()))
			{
				int selectedPropertyIndex = this.transformationsTable.getSelectionModel().getSelectedIndex();
				if (selectedPropertyIndex >= 0)
				{
					this.removeTransformationButton.setDisable(false);
					this.addTransformationButton.setDisable(true);
				}
				else
				{
					this.addTransformationButton.setDisable(true);
					System.out.println("cView4: Index outside data: " + selectedPropertyIndex);
				}
			}
			else
			{
				System.out.println("Unexpected button.");
			}
		}
		catch (Exception except)
		{
			System.out.println("cView4: Exception in handle():" + except);
		}

	}

	public Map<String, List<String>> innerJoin()
	{
		Iterator<Properties> iter = this.transformationsTableDataList.iterator();
		Iterator<Properties2> iter2 = this.propertiesTableDataList.iterator();
		List<String> tempColumnsList;
		Map<String, List<String>> outMap;
		Properties prop;
		Properties2 prop2;
		int removeIndex;
		String mapName;

		removeIndex = 0;
		tempColumnsList = new LinkedList<>(this.propertiesTableColumnNames);
		tempColumnsList.remove(removeIndex);
		removeIndex = tempColumnsList.size();
		tempColumnsList.addAll(this.transformationsTableColumnNames);
		tempColumnsList.remove(removeIndex);

		outMap = new HashMap<>();
		for (int i = 0; i < tempColumnsList.size(); i++)
		{
			outMap.put(tempColumnsList.get(i), new LinkedList<>());
		}

		assert this.transformationsTableDataList.size() == this.propertiesTableDataList.size() : "The data in these two tables must correspond, an is inner-joinnable on columns  columnName and mapName.";

		while (iter.hasNext())
		{
			assert iter2.hasNext() : "data in transformationsTableDataList and propertiesTableDataList must be one to one.";

			prop = iter.next();
			prop2 = iter2.next();

			mapName = this.mapNameTextArea.getText();
			outMap.get("mapName").add(mapName); //primary key uniquely identifying this data set
			outMap.get("columnName").add(prop.getColumnName());
			outMap.get("fileReference").add(prop2.getFileReference());
			outMap.get("type").add(prop2.getType());
			outMap.get("transformation").add(prop.getTransformation());
			//outMap.get("filename").add(); //filename only available later so leave for now
		}

		return outMap;
	}

	public void writeMap() throws Exception
	{
		//innerJoin on column columnName 

		//this.transformationsTableDataList, this.propertiesTableDataList;
		Map<String, List<String>> inMap;
		List<List<String>> valuesLists, columnsLists;
		List<String> tableNamesList, columnNames, tempColumnList, tempValueList;
		String value;

		inMap = innerJoin();
		columnNames = new LinkedList<>(inMap.keySet());
		valuesLists = new LinkedList<>();
		columnsLists = new LinkedList<>();
		tableNamesList = new LinkedList<>();

		tempColumnList = inMap.get(columnNames.get(0));

		for (Iterator<String> it = tempColumnList.iterator(); it.hasNext(); it.next())
		{
			tableNamesList.add(this.mapsTableName);
		}

		for (int i = 0; i < tempColumnList.size(); i++)
		{
			tempValueList = new LinkedList<>();
			for (int y = 0; y < columnNames.size(); y++)
			{
				tempColumnList = inMap.get(columnNames.get(y));
				value = tempColumnList.get(i);
				tempValueList.add(value);
			}
			columnsLists.add(new LinkedList<>(columnNames));
			valuesLists.add(tempValueList);
		}

		this.controller.write(this, tableNamesList, columnsLists, valuesLists);

		//set the new maps
		inMap = ((cMySQLController)this.controller).readValue(this.mapsTableName);
		System.out.println("***view4: good so far:" + inMap);
		Set<String> mapNamesSet = new HashSet<>();
		List<String> list = inMap.get("mapName");
		System.out.println("***view4: null data?" + list);
		mapNamesSet.addAll(list);
		System.out.println("***view4: addable:" + mapNamesSet);
		this.mapNamesList.addAll(mapNamesSet);
	}

	public void writeTransformations() throws Exception
	{
		Iterator<Properties> iter = this.transformationsTableDataList.iterator();
		List<String> outValues, outTableNamesList;
		Properties prop;

		List<List<String>> outValuesList = new LinkedList<>(), outColumnsList;
		LinkedList<String> tableColumnsList = new LinkedList<>(this.transformationsTableColumnNames);
		tableColumnsList.removeFirst();
		outTableNamesList = new LinkedList<>();
		outColumnsList = new LinkedList<>();

		Optional<String> result = this.mapNameDialog.showAndWait();

		if (result.isPresent())//use the name when you write to controller- do it like we do table name and populate for every row
		{
			System.out.println("Your name: " + result.get());
			while (iter.hasNext())
			{
				prop = iter.next();
				outValues = new LinkedList<>();
				outValues.add(prop.getColumnName());
				outValues.add(prop.getTransformation());
				outValuesList.add(outValues);
				outColumnsList.add(tableColumnsList);
				outTableNamesList.add(this.transformationsTableName);
			}
			this.controller.write(this, outTableNamesList, outColumnsList, outValuesList); //Only write to DataTrain tables when user asks for file write

			this.saveMapButton.setDisable(true);
			this.generateFileButton.setDisable(false);
			this.addTransformationButton.setDisable(true);
			this.removeTransformationButton.setDisable(true);
			this.transformationsComboBox.disarm();
		}
		else
		{
			System.out.println("New Map not created because no name was provided.");
			Alert alert = new Alert(Alert.AlertType.WARNING);
			alert.setTitle("Warning");
			alert.setHeaderText("Problem creating your new Map");
			alert.setContentText("New Map not created because no name was provided.");
			alert.showAndWait();
		}

	}

	public static class Properties
	{

		private final SimpleStringProperty columnName;
		private final SimpleStringProperty transformation;

		private Properties(String clmName, String tr)
		{
			this.transformation = new SimpleStringProperty(tr);
			this.columnName = new SimpleStringProperty(clmName);
		}

		public String getTransformation()
		{
			return transformation.get();
		}

		public void setTrasnformation(String tr)
		{
			transformation.set(tr);
		}

		public String getColumnName()
		{
			return columnName.get();
		}

		public void setColumnName(String cName)
		{
			columnName.set(cName);
		}
	}

	/*
	@Override
	public void onApplicationWrite(String tableName)
	{
		if (tableName.equals(this.propertiesTableName))
		{
			if (this.stage == null)
				return;

			this.start(this.stage);
		}
	}
	 */
	@Override
	public void onApplicationWrite(String tableName, Map<String, List<String>> data)
	{
		if (tableName.equals(this.propertiesTableName))
		{
			if (this.stage == null || data == null)
				return;
			List<String> functionalTableColumns = new LinkedList<>(this.propertiesTableColumnNames);
			functionalTableColumns.remove(0);
			if (data.keySet().containsAll(functionalTableColumns) && functionalTableColumns.containsAll(data.keySet()))
			{
				this.propertiesTableDataList.clear();
				this.transformationsTableDataList.clear();

				Iterator<String> iter = functionalTableColumns.iterator();
				Properties2 prop;
				List<Properties2> propData = new LinkedList<>();
				String columnName, fileRef, type, attribute;
				List<String> list;

				Set<String> keys = data.keySet();
				Iterator<String> keyIter = keys.iterator();
				attribute = keyIter.next();
				int size = data.get(attribute).size();
				while (keyIter.hasNext())
				{
					attribute = keyIter.next();
					assert data.get(attribute).size() == size;
				}

				columnName = fileRef = type = "null";
				for (int i = 0; i < size; i++)
				{
					keyIter = keys.iterator();
					while (keyIter.hasNext())
					{
						attribute = keyIter.next();
						list = data.get(attribute);
						assert list != null : "Every key of data must map to a list with at least a single entry in it!";
						switch (attribute)
						{
							case "columnName":
								columnName = list.get(i);
								break;
							case "fileReference":
								fileRef = list.get(i);
								break;
							case "type":
								type = list.get(i);
								break;
							case "mapName":
								this.mapNameTextArea.setText(list.get(i));
								break;
							default:
								System.out.println("Unknown unexpected attribute '" + attribute + "'");
								System.exit(1);
						}
					}
					prop = new Properties2(columnName, fileRef, type);
					propData.add(prop);
				}

				this.propertiesTableDataList.setAll(propData);
				this.start(this.stage);
			}
			else
			{
				System.out.println("Ignoring write of unexpected columns.");
			}

		}
	}

	public void onViewWrite(String table, Map<String, List<String>> data)
	{
		assert table != null : "cView3: onViewWrite(): controller should never call this method without a table name set.";
		assert data != null : "cView3: onViewWrite(): controller should never call this method without data set.";
		assert this.stage != null : "cView3: onViewWrite(): if cView object used with controller, stage should be set on which cView renders widgets, etc.";

		/*
		if (table.equals(this.mapsTableName))
		{
			List<String> dataColumn = data.get(this.mapsTableMapNameColumnName);
			Integer lastEntryIndex = dataColumn.size() - 1;
			String mapName = dataColumn.get(lastEntryIndex);
			this.mapNameTextArea.setText(mapName);
		}
		 */
		if (table.equals(this.mapsTableName))
		{
			System.out.println("********cView4: i dont like to loop back on self");
		}
		else
		{
			System.out.println("cView4: onViewWrite():Ignoring uninteresting table name '" + table + "'.");
		}
	}

	public static void main(String[] args)
	{
		System.out.println("Launching...");
		launch(args);
		System.out.println("Launched.");
	}
}
