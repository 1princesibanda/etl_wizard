/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.datatrain.DataTrain;

import com.gew.datatrain.Controller.mysqlcontroller.IController;
import com.gew.datatrain.Controller.mysqlcontroller.cMySQLController;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import com.gew.datatrain.Controller.mysqlcontroller.IApplication;
import com.gew.datatrain.ui.cView3;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Uses config file or CLAP or UI for setting options and config. Will interface with UI using relational DB, with a direct connection to DB being kept by the controller. Thus this app needs a
 * controller object which it relies on to write data into the DB, and for retrieving data from the DB. Retrieving new data can be synchronous or asynchronous. If asynchronous, call Controller object
 * once and right away get yes/no response from controller about whether there is new data(of a particular table(s)) in DB or not. Controller also allows for UI and this App to be even driven so that
 * UI and App are getting calls made on their call-backs by the controller. Thus Controller can take option to bypass writting DB and forward the data directly so that data written by either UI or App
 * is read right away by the other.
 *
 * @author 0400626
 */
public class cDataTrain implements IApplication
{

	private IController controller;
	private static final String TABLE = "atable";
	private static final String DATABASE_NAME = "dbName";
	private static final String HOST = "dbName";
	private static final String USERNAME = "dbName";
	private static final String PASSWORD = "dbName";
	private static final String DEFAULT_FILENAME = "out.txt";
	private String fileName;
	private List<String> mapList;

	private static final String DEFAULT_TRANSFORMATIONS_TABLE_NAME = "tblTransformations", DEFAULT_QUERY_TABLE_NAME = "tblQueries", DEFAULT_PROPERTIES_TABLE_NAME = "tblProperties", DEFAULT_MAPS_FILETABLE_NAME="tblMapsFilenames";

	private Map<String, List<String>> sqlData, propertiesData, transformationsData, filenameData;

	private String sql;

	private String sqlTableName, transformationTableName, propertyTableName, fileTableName;

	public cDataTrain(IController controller)
	{
		this();
		this.controller = controller;
		//this.controller.register(this, IController.TYPE.APPLICATION);
	}

	public cDataTrain()
	{
		this.sql = null;
		this.controller = null;
		this.sqlTableName = this.transformationTableName = this.propertyTableName = null;

		this.sqlData = this.propertiesData = this.transformationsData = this.filenameData = null;

		this.fileName = null;
		this.fileTableName = null;
		this.mapList = new LinkedList<>();
		init();
	}

	private void init()
	{
		//this(cDataTrain.HOST, cDataTrain.DATABASE_NAME, cDataTrain.USERNAME, cDataTrain.PASSWORD);
		this.fileName = this.fileName == null ? cDataTrain.DEFAULT_FILENAME : this.fileName;
		this.sqlTableName = this.sqlTableName == null ? cDataTrain.DEFAULT_QUERY_TABLE_NAME : this.sqlTableName;
		this.propertyTableName = this.propertyTableName == null ? cDataTrain.DEFAULT_PROPERTIES_TABLE_NAME : this.propertyTableName;
		this.transformationTableName = this.transformationTableName == null ? cDataTrain.DEFAULT_TRANSFORMATIONS_TABLE_NAME : this.transformationTableName;
		this.fileTableName = this.fileTableName == null ? cDataTrain.DEFAULT_MAPS_FILETABLE_NAME : this.fileTableName;
	}

	//@Override
	public void onViewRead(String database, String table)
	{
		System.out.println("DataTrain knows a View read table " + database + " and table " + table);
	}

	public void onViewWrite(String database, String table)
	{
		System.out.println("DataTrain knows a View wrote database " + database + " and table " + table);
	}

	@Override
	public void onViewWrite(String tableName, Map<String, List<String>> data)
	{
		if (data == null)
		{
			System.out.println("CDataTrain: no data, nothing to do.");
			return;
		}
		if (tableName == null)
		{
			System.out.println("CDataTrain: No table name- nothing to do.");
			return;
		}
		System.out.println("CDataTrain: got tableName '" + tableName + "'");

		Map<String, List<Integer>> transformationGrouping;
		String mapString;

		if (tableName.equals(this.sqlTableName))
		{
			List<String> columnNames = new LinkedList<>();
			String key = "querySql";
			columnNames.add(key);

			if (data.keySet().containsAll(columnNames) && columnNames.containsAll(data.keySet()))
			{
				if (this.sql == null)
				{
					System.out.println("CDataTrain: set sql value '" + data.get(key).get(0) + "'");
					List<String> list = data.get(key);
					this.sql = list.get(list.size() - 1);
				}
				else if (this.sql != null)
				{
					System.out.println("CDataTrain: overwrite sql value to '" + this.sql + "'");
					List<String> list = data.get(key);
					this.sql = list.get(list.size() - 1);
					//There is an overwrite on this input, so null the other inputs to indicate they are pending new values too
					this.propertiesData = this.transformationsData = null;
				}

				if ((this.sql != null) && (this.propertiesData != null) && (this.transformationsData != null))
				{
					//process inputs
					transformationGrouping = processInput();
					mapString = getMapString(transformationGrouping);
					this.mapList.add(mapString);
				}
			}
			else
			{
				System.out.println("cDataTrain: sqlTable input, but pending either properties data, transformations data or both");
			}
		}
		else if (tableName.equals(this.propertyTableName))
		{
			List<String> columnNames = new LinkedList<>();
			columnNames.add("columnName");
			columnNames.add("fileReference");
			columnNames.add("type");

			if (data.keySet().containsAll(columnNames) && columnNames.containsAll(data.keySet()))
			{

				if (this.propertiesData == null)
				{
					System.out.println("CDataTrain: set properties data .");
					this.propertiesData = data;
				}
				else if (this.propertiesData != null)
				{
					System.out.println("CDataTrain: overwrite properties data .");
					this.propertiesData = data;
					//There is an overwrite on this input, so clear the other inputs to indicate they are pending new values too
					this.sql = null;
					this.transformationsData = null;
				}

				if ((this.sql != null) && (this.propertiesData != null) && (this.transformationsData != null))
				{
					//process inputs
					transformationGrouping = processInput();
					mapString = getMapString(transformationGrouping);
					this.mapList.add(mapString);
				}
			}
			else
			{
				System.out.println("cDataTrain: propertyTable input, but pending either sql data, transformations data or both");
			}
		}
		else if (tableName.equals(this.transformationTableName))
		{
			//number of transformations should be same as number of properties because there is a transformation for each property
			List<String> columnNames = new LinkedList<>();
			columnNames.add("columnName");
			columnNames.add("transformation");

			if (data.keySet().containsAll(columnNames) && columnNames.containsAll(data.keySet()))
			{
				if (this.transformationsData == null)
				{
					System.out.println("CDataTrain: set transformations data .");
					this.transformationsData = data;
				}
				else if (this.transformationsData != null)
				{
					System.out.println("CDataTrain: overwrite transformations data .");
					this.transformationsData = data;
					//There is an overwrite on this input, so clear the other inputs to indicate they are pending new values too
					this.sql = null;
					this.propertiesData = null;
				}

				if ((this.sql != null) && (this.propertiesData != null) && (this.transformationsData != null))
				{
					//process inputs
					transformationGrouping = processInput();
					mapString = getMapString(transformationGrouping);
					this.mapList.add(mapString);
				}
			}
			else
			{
				System.out.print("cDataTrain: transformationTable input, but pending either properties data, sql data or both");
			}
		}
		else if (tableName.equals(this.fileTableName))
		{
			List<String> columnNames = new LinkedList<>();
			columnNames.add("filename");

			if (data.keySet().containsAll(columnNames) && columnNames.containsAll(data.keySet()))
			{
				if (this.filenameData == null)
				{
					System.out.println("CDataTrain: set transformations data .");
					this.filenameData = data;
				}
				else if (this.filenameData != null)
				{
					System.out.println("CDataTrain: overwrite fileTableName data .");
					this.filenameData = data;
					//There is an overwrite on this input, so clear the other inputs to indicate they are pending new values too
					this.sql = null;
					this.propertiesData = null;
					this.transformationsData = null;
				}

				if ((this.filenameData != null) && (this.sql != null) && (this.propertiesData != null) && (this.transformationsData != null))
				{
					writeMapFile();
				}
			}
			else
			{
				System.out.print("cDataTrain: fileTableName input, but pending at least properties data, transformations data or sql data.");
			}
		}
		else
		{
			System.out.println("cDataTrain: Ignoring non-interesting write of table '" + tableName + "'.");
		}
	}

	private Map<String, List<Integer>> processInput()
	{
		//
		Iterator<String> propertiesDataIter, transformationsListIter, transformationsSetIter;
		String dataPivotKey, transformationKey, transformation, columnName; // ie the key that is in both properties data and 
		List<String> dataPivotList, transformationList, propertiesColumnNameList;
		List<Integer> transformationPropertiesList;
		int totalRows, rowIndex;
		Map<String, List<Integer>> outMap;
		Set<String> transformationsSet;

		//init
		outMap = new HashMap<>();
		dataPivotKey = "columnName";
		transformationKey = "transformation";
		dataPivotList = this.transformationsData.get(dataPivotKey);
		transformationList = this.transformationsData.get(transformationKey);
		propertiesColumnNameList = this.propertiesData.get(dataPivotKey);
		transformationsListIter = transformationList.iterator();
		totalRows = transformationList.size();
		rowIndex = 0;

		assert new HashSet<>(dataPivotList).containsAll(dataPivotList) : "There are no duplicate entries, ie no two columns have the same name.";

		assert dataPivotList.containsAll(propertiesColumnNameList) && propertiesColumnNameList.containsAll(dataPivotList) : "The pivot list should contain the same data as in in both transformations data and properties data";

		//map transformations to List of propeties that have the same transformation
		//map transformations to empty lists;
		while (transformationsListIter.hasNext())
		{
			outMap.put(transformationsListIter.next(), new LinkedList<>());
		}

		assert !outMap.values().contains(null) : "After the above loop has finished, there should be no key that maps to null.";

		transformationsSet = outMap.keySet();
		transformationsSetIter = transformationsSet.iterator();
		while (transformationsSetIter.hasNext())
		{
			transformation = transformationsSetIter.next();
			rowIndex = transformationList.indexOf(transformation);
			columnName = dataPivotList.get(rowIndex);
			transformationPropertiesList = outMap.get(transformation);

			assert propertiesColumnNameList.contains(columnName) : "propertiesColumnNameList and transformationList should contain same data - thats how we know which property has which transformation.";
			transformationPropertiesList.add(propertiesColumnNameList.indexOf(columnName));
			for (int i = ++rowIndex; i < totalRows; i++)
			{
				if (transformationList.get(i).equals(transformation))
				{
					columnName = dataPivotList.get(i);
					assert propertiesColumnNameList.contains(columnName) : "propertiesColumnNameList and transformationList should contain same data - thats how we know which property has which transformation.";
					transformationPropertiesList.add(propertiesColumnNameList.indexOf(columnName));
				}
			}

		}

		//assert each transformation is a key
		Set<String> keySet = outMap.keySet();
		assert keySet.equals(new HashSet<>(transformationList));
		//assert that no two keys map to intersecting lists
		Collection<List<Integer>> coll = outMap.values();
		Object[] arr = coll.toArray();
		for (int i = 0, y; i < arr.length - 1; i++)
		{
			for (y = i + 1; y < arr.length; y++)
			{
				assert !((List<Integer>) arr[i]).equals((List<Integer>) arr[y]) : "There should be no two transformation keys that map to lists that intersect since the map is supposed to group the properties in the lists by transformation so that each property is associated with a single transformation.";
			}
		}
		return outMap;
	}

	private String getMapString(Map<String, List<Integer>> transformationGrouping)
	{

		if (transformationGrouping == null)
		{
			throw new IllegalArgumentException("argument transformationGrouping should not be null.");
		}

		assert this.propertiesData != null : "Do not call this function getMapString() before needed propertiesData is available.";
		assert this.transformationsData != null : "Do not call this function getMapString() before needed transformationsData is available.";
		assert this.sql != null : "Do not call this function getMapString() before needed sql is available.";

		//Declare
		int totalRows;
		Set<String> keySet, transformationGroupingKeySet;
		List<String> propertyList, columnNameList, fileReferenceList, typeList;
		Iterator<String> keyIter, transformationGroupingKeySetIter;
		String columnName, fileReference, type, property, properties, transformation, mapString;
		List<Integer> transformationGroupingList;
		Iterator<Integer> transformationGroupingListIter;
		Integer rowIndex;

		//Init
		keySet = this.propertiesData.keySet();
		keyIter = keySet.iterator();
		propertyList = this.propertiesData.get(keyIter.next());
		totalRows = propertyList.size();
		transformationGroupingKeySet = transformationGrouping.keySet();
		transformationGroupingKeySetIter = transformationGroupingKeySet.iterator();
		properties = "";
		mapString = "<Map Name=\"SignalResult\" PrintDataSet=\"false\">\n"
			    + "    <Input>\n"
			    + "       <Sql>" + this.sql + "</Sql>\n"
			    + "    </Input>\n";

		//assert all entries have same size
		while (keyIter.hasNext())
		{
			propertyList = this.propertiesData.get(keyIter.next());
			assert totalRows == propertyList.size();
		}

		//Go through each transformation and build properties
		while (transformationGroupingKeySetIter.hasNext())
		{
			transformation = transformationGroupingKeySetIter.next();

			properties = "    <Transformation=" + transformation + "/>\n"
				     + "    <Output>\n"
				     + "        <container Type=\"ENTITY\"> \n"
				     + "            <properties>\n";

			columnNameList = this.propertiesData.get("columnName");
			fileReferenceList = this.propertiesData.get("fileReference");
			typeList = this.propertiesData.get("type");

			transformationGroupingList = transformationGrouping.get(transformation);
			transformationGroupingListIter = transformationGroupingList.iterator();

			//Build properties
			while (transformationGroupingListIter.hasNext())
			{
				rowIndex = transformationGroupingListIter.next();

				columnName = columnNameList.get(rowIndex);
				fileReference = fileReferenceList.get(rowIndex);
				type = typeList.get(rowIndex);

				property = "                <property>\n";
				property += "                    <name>" + columnName + "</name>\n";
				property += "                    <class>" + type + "</class>\n";
				property += "                    <fileReference>" + fileReference + "</fileReference>\n";
				property += "                </property>\n";
				properties += property;
			}

			properties += "            </properties>\n";
			mapString += properties;
			mapString += "        </container>\n"
				     + "    </Output>\n";
		}
		mapString += "</Map>\n";

		return mapString;
	}

	public void writeMapFile()
	{
		//check all the nulls
		
		if(this.filenameData != null)
		{
			String columnName = "filename";
			List<String> filenameList = filenameData.get(columnName);
			assert filenameList != null : "filenameData should is expected to have mapping for key filename. filenameData:"+this.filenameData;
			assert filenameList.size() == 1;
			this.fileName = filenameList.get(0);
		}

		Iterator<String> mapIter;
		FileWriter fileWriter;
		PrintWriter printWriter;

		mapIter = this.mapList.iterator();
		printWriter = null;
		
		try
		{
			fileWriter = new FileWriter(this.fileName);
			printWriter = new PrintWriter(fileWriter);
		}
		catch (Exception io)
		{
			System.out.println(io);
		}

		while (mapIter.hasNext())
		{
			printWriter.print(mapIter.next());
		}
		
		printWriter.close();

	}

	//Can start cDataTrain directly here in main() (or on cmdline if you add CLAP) or from UI
	public static void main(String[] args)
	{
		//
	}
}
