grammar TableDefinition;
//import CommonLexerRules;

prog:   line+                                                                                                                                   # allTables ;  

line:    CREATE TABLE ifnotexists? tableName '(' columnDefinitionList ')'  (ENGINE '=' INNODB)? COLON                                     		# tableDefinition
//    |    NEWLINE                                                                                                                              # blankTableDefinition 
    |   COMMENT+                                                                                                                                # comments;

columnDefinitionList:   columnDefinition                                                                                                        # columnDef
                    | 	PRIMARY KEY '(' columnNameList ')'											# primaryKeyListing
                    |   columnDefinition COMMA columnDefinitionList                                                                             # columnDefList ;

//columnDefinition:   ID sqlDataType (AUTO_INCREMENT|(NOT NULL))? ;
columnDefinition:   ID sqlDataType AUTO_INCREMENT? (NOT NULL)? 
| ID sqlDataType
                |   ID sqlDataType DEFAULT literal ;         //We do not yet allow expressions in literals                                                                                           

columnNameList: ID                                                                                                                              # primaryKeyColumn
              | ID COMMA columnNameList                                                                                                         # primaryKeyColumnList ;
			  
sqlDataType: directType                                                                                                                         //# mySqlType
           | precisionType                                                                                                                    ; //# mySqlPrecisionType ;

directType: TINYINT | SMALLINT | YEAR | INT_ | BIGINT | UNSIGNED | FLOAT | DOUBLE | NUMERIC | DECIMAL | DATETIME | TIMESTAMP | TIME | DATE | BOOLEAN ;

precisionType: BIT OPENINGROUNDBRACE INT CLOSINGROUNDBRACE 
			|	VARCHAR OPENINGROUNDBRACE NUMERIC_LITERAL CLOSINGROUNDBRACE ;

tableName:  ID ;
                                                                                 
comment : COMMENT ;

ifnotexists:	IF NOT EXISTS ;

literal: STRING_LITERAL | NUMERIC_LITERAL | DATE_AND_TIME_LITERAL | HEXADECIMAL_LITERAL | BITVALUE_LITERAL | BOOLEAN_LITERAL | NULL ;

STRING_LITERAL:   ['].*?['] | ["].*?["] ;
NUMERIC_LITERAL: [-+]?[0-9]*'.'?[0-9]+([eE][-+]?[0-9]+)?  ;
DATE_AND_TIME_LITERAL: (DATE|ODBC_date|TIME|ODBC_time|TIMESTAMP|ODBC_timestamp) ('\''.*?'\'' | '"'.*?'"') ;
HEXADECIMAL_LITERAL: ([xX]['][A-F0-9]['])|('0x'[0-9A-F]) ;
BITVALUE_LITERAL:    ([bB]['][0-9A-F]['])|('0b'[0-9A-F]) ;
BOOLEAN_LITERAL: TRUE | FALSE ;
ODBC_date:  [dD] ;
ODBC_time:  [tT] ;
ODBC_timestamp: [tT][sS] ;
MYINT:[0-9]+ ;

IF:	[iI][fF] ;
NOT:	[nN][oO][tT] ;
EXISTS:	[eE][xX][iI][sS][tT][sS] ;
TRUE:   [tT][rR][uU][eE] ;
FALSE:  [fF][aA][lL][sS][eE] ;
PRIMARY:    [pP][rR][iI][mM][aA][rR][yY] ;
KEY:    [kK][eE][yY] ;
CREATE: [cC][rR][eE][aA][tT][eE] ;
TABLE:  [tT][aA][bB][lL][eE] ;
COLON:  ';' ;
COMMA: ',' ;
VARCHAR: [vV][aA][rR][cC][hH][aA][rR] ;
CLOSINGROUNDBRACE:  ')' ;
OPENINGROUNDBRACE: '(' ;
TINYINT: [tT][iI][nN][yY][iI][nN][tT] ;
SMALLINT:   'SMALLINT' ;
YEAR:   [yY][eE][aA][rR] ;
INT_:   [iI][nN][tT] ;
BIGINT: [bB][iI][gG][iI][nN][tT] ;
UNSIGNED:   [uU][nN][sS][iI][gG][nN][eE][dD] ;
FLOAT:  [fF][lL][oO][aA][tT] ;
DOUBLE: [dD][oO][uU][bB][lL][eE] ;
NUMERIC:    [nN][uU][mM][eE][rR][iI][cC] ;
DECIMAL:    [dD][eE][cC][iI][mM][aA][lL] ;
DATETIME:   [dD][aA][tT][eE][tT][iI][mM][eE] ;
TIMESTAMP:  [tT][iI][mM][eE][sS][tT][aA][mM][pP] ;
TIME:   [tT][iI][mM][eE] ;
DATE:   [dD][aA][tT][eE] ;
BIT:    [bB][iI][iT] ;
BOOLEAN:	[bB][oO][oO][lL]([eE][aA][nN])? ;
ENGINE:	[eE][nN][gG][iI][nN][eE] ;
INNODB:[iI][nN][nN][oO][dD][bB] ;
AUTO_INCREMENT: [aA][uU][tT][oO]'_'[iI][nN][cC][rR][eE][mM][eE][nN][tT] ;
NULL: [nN][uU][lL][lL] ;
DEFAULT: [dD][eE][fF][aA][uU][lL][tT] ;
ID  :   [a-zA-Z_]+ ;      // match identifiers
INT :   [0-9]+ ;         // match integers
WS  :   [ \r\n\t]+ -> skip ; // toss out whitespace
COMMENT:    '/*' .*? '*/' ; //The ? makes for a non-greedy syntax. If removed, expression is greedy
//ANYTEXT:    [*a-zA-Z0-9\]!$'"+=^%@~`,/:;#&\-]+ ;
//ANYTEXT:    [!WS]+ ;


//NEWLINE:'\r'? '\n' ;     // return newlines to parser (end-statement signal)
