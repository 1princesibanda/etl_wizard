/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.datatrain.ui;

import com.gew.datatrain.Controller.mysqlcontroller.IController;
import com.gew.datatrain.Controller.mysqlcontroller.ISQLParser;
import com.gew.datatrain.Controller.mysqlcontroller.IView;
import com.gew.datatrain.Controller.mysqlcontroller.cMySQLController;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Remember to use a controller object to write data out of the UI, and to use a non-blocking call if called from main app thread, otherwise call from a worker thread
 *
 * @author 0400626
 */
public final class cView1 extends IDataTrainView implements EventHandler<Event>
{

	private static final String LOGIN_CREDENTIALS_TABLENAME = "loginCredentials";

	private static final List<String> DEFAULT_LOGIN_CREDENTIALS_TABLE_COLUMNS = Arrays.asList("loginCredentialsId", "username", "password", "filename", "hostname");

	public static final int UNSUPPORTED_CONTROLLER_USED = 1;

	private boolean INITIALISED;

	Pane pane;
	Scene scene;
	Stage stage;
	ObservableList<String> listOfDataSources;
	ComboBox comboBox;
	Bounds bounds;
	Text scenetitle;
	private Text connectionOutcome = null;
	Button nextButton, testButton;
	Label hostLabel, fnameLabel, userLabel, pwLabel;
	TextField hostTextField, fnameTextField, userTextField, pwTextField;
	IController controller;
	Task<Void> task;
	Thread thread;
	IDataTrainView nextView;

	private String loginCredentialsTableName;
	private List<String> loginCredentialsTableColumns, loginCredentialsTableData;

	public cView1(Pane pane)
	{
		this();
		this.pane = pane;
		//this.scene = new Scene(this.pane, 200, 200);
	}

	public cView1()
	{
		this.INITIALISED = false;

		this.pane = new Pane();
		this.scene = new Scene(this.pane, 1300, 500);

		this.scenetitle = new Text("From");

		this.listOfDataSources = FXCollections.observableArrayList("Relational Database", "CSV File"); //, "JSON File"
		//position combobox using Region's static void positionInArea() method, which uses the Node's set bounds(ie width and height) to position the Node in the Region's area.
		this.comboBox = new ComboBox(this.listOfDataSources);
		this.bounds = null;

		this.loginCredentialsTableName = null;

		this.loginCredentialsTableColumns = null;

		this.loginCredentialsTableData = null;

		this.nextButton = new Button("Next");

		this.hostLabel = new Label("Host:");

		this.hostTextField = new TextField();

		this.fnameLabel = new Label("Filename:");

		this.fnameTextField = new TextField();

		this.userLabel = new Label("User Name:");

		this.userTextField = new TextField();

		this.pwLabel = new Label("Password:");

		this.pwTextField = new PasswordField();

		this.testButton = new Button("Test");

		this.connectionOutcome = new Text();

		this.task = new Task<Void>()
		{
			@Override
			protected Void call() throws Exception
			{
				while (!isCancelled())
				{
					if ((hostTextField.getText().equals("")) || (fnameTextField.getText().equals("")) || (userTextField.getText().equals("")) || (pwTextField.getText().equals("")))
					{
						testButton.setDisable(true);
					}
					else
					{
						testButton.setDisable(false);
					}
					Thread.sleep(100); //arguments are milliseconds 
				}
				return null;
			}
		};

		this.controller = null;
		super.nextView = null;
		this.stage = null;

	}

	public cView1(IController controller)
	{
		this();
		this.controller = controller;
		System.out.println("View1 Setting controller...");
	}

	public cView1(IController controller, IDataTrainView nextView)
	{
		this();
		this.controller = controller;
		super.nextView = nextView;
	}

	@Override
	public void init()
	{
		this.INITIALISED = true;

		this.scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 20));
		this.scenetitle.setId(IDataTrainView.uniqueId());
		this.scenetitle.setX(100);
		this.scenetitle.setY(100);

		this.connectionOutcome.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 20));
		this.connectionOutcome.setId(IDataTrainView.uniqueId());
		this.connectionOutcome.setX(600);
		this.connectionOutcome.setY(260);
		this.connectionOutcome.setVisible(false);

		//get the bounds so that we can position comboBox using layout area that is just sufficient and tight.
		this.bounds = this.comboBox.getLayoutBounds();
		Region.positionInArea(this.comboBox, 100, 150, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.comboBox.setId(IDataTrainView.uniqueId());
		this.comboBox.setOnAction(this);
		this.comboBox.setId(IDataTrainView.uniqueId());

		this.nextButton.setDisable(true);
		this.testButton.setDisable(true);

		bounds = this.nextButton.getLayoutBounds();
		Region.positionInArea(this.nextButton, 1000, 400, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.nextButton.setOnMouseClicked(this);
		this.nextButton.setId(IDataTrainView.uniqueId());

		bounds = this.testButton.getLayoutBounds();
		Region.positionInArea(this.testButton, 900, 400, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.testButton.setOnMouseClicked(this);
		this.testButton.setId(IDataTrainView.uniqueId());

		bounds = this.hostLabel.getLayoutBounds();
		Region.positionInArea(this.hostLabel, 100, 210, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.hostLabel.setId(IDataTrainView.uniqueId());
		bounds = this.fnameLabel.getLayoutBounds();
		Region.positionInArea(this.fnameLabel, 100, 260, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.fnameLabel.setId(IDataTrainView.uniqueId());
		bounds = this.userLabel.getLayoutBounds();
		Region.positionInArea(this.userLabel, 100, 310, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.userLabel.setId(IDataTrainView.uniqueId());
		bounds = this.pwLabel.getLayoutBounds();
		Region.positionInArea(this.pwLabel, 100, 360, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.pwLabel.setId(IDataTrainView.uniqueId());

		bounds = this.hostTextField.getLayoutBounds();
		Region.positionInArea(this.hostTextField, 200, 210, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.hostTextField.setId(IDataTrainView.uniqueId());
		this.hostTextField.setOnKeyTyped(this);
		bounds = this.fnameTextField.getLayoutBounds();
		Region.positionInArea(this.fnameTextField, 200, 260, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.fnameTextField.setId(IDataTrainView.uniqueId());
		this.fnameTextField.setOnKeyTyped(this);
		bounds = this.userTextField.getLayoutBounds();
		Region.positionInArea(this.userTextField, 200, 310, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.userTextField.setId(IDataTrainView.uniqueId());
		this.userTextField.setOnKeyTyped(this);
		bounds = this.pwTextField.getLayoutBounds();
		Region.positionInArea(this.pwTextField, 200, 360, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.pwTextField.setId(IDataTrainView.uniqueId());
		this.pwTextField.setOnKeyTyped(this);

		this.loginCredentialsTableName = this.loginCredentialsTableName == null ? cView1.LOGIN_CREDENTIALS_TABLENAME : this.loginCredentialsTableName;

		this.loginCredentialsTableColumns = this.loginCredentialsTableColumns == null ? cView1.DEFAULT_LOGIN_CREDENTIALS_TABLE_COLUMNS : this.loginCredentialsTableColumns;

		this.thread = null;
	}

	//make new scene using the pane
	@Override
	public void start(Stage stage)
	{
		if (!this.INITIALISED)
			this.init();

		//configure the stage
		if (this.stage == null)
		{
			this.stage = stage;
		}
		stage.setTitle("Text Data Format Converter");
		stage.setMaxHeight(500);
		stage.setMaxWidth(1300);
		stage.setMinHeight(500);
		stage.setMinWidth(1300);
		stage.setFullScreen(false);
		stage.setResizable(false); //Toggles availability of Windows minimise and maximise. if set to false and setMaximized() is set, then it is fixed to the setting of setMaximized()
		stage.setMaximized(false);
		//stage.toFront(); Not really data- these are meant for use at runtime
		stage.setIconified(false); //Puts the window down at the bottom task bar where the open programs are. Takes precedence over toBack() and toFront();
		stage.setFullScreenExitHint("This is exit hint");
		stage.setAlwaysOnTop(false);

		this.pane.getChildren().add(this.scenetitle);
		this.pane.getChildren().add(this.comboBox);
		this.pane.getChildren().add(this.nextButton);

		stage.setScene(this.scene);
	}

	/*
    public void OracleComboboxHandle()
    {
    }
	 */
	@Override
	public void handle(Event e)
	{
		Node source = (Node) e.getSource(); //Should always be a Node on the Scene graph
		try
		{
			//Use source.getId() to distinguish between nodes of the same type. You may even setId in the constructor and then get it here.
			if (source instanceof Button)
			{
				//These below should hold if they were created in the constructor as they should with all other objects
				assert source != null;
				assert testButton != null;
				//true if all scene nodes have had their nodeId's set as they should in the init() method            
				assert source.getId() != null;
				assert testButton.getId() != null;

				if (source.getId().equals(this.testButton.getId()))
				{

					//Ask the actual application(not the GUI) to connect and give us feedback
					//this.controller.write(this.fnameTextField.getText(), this.LOGIN_CREDENTIALS_TABLENAME, this.hostTextField.getText(), this.fnameTextField.getText(), this.userTextField.getText(), this.pwTextField.getText());
					try
					{
						this.controller.setMySQLCredentials(this.hostTextField.getText(), this.fnameTextField.getText(), this.userTextField.getText(), this.pwTextField.getText());
						this.connectionOutcome.setFill(Color.GREEN);
						this.connectionOutcome.setText("Successfully connected!");
						this.connectionOutcome.setVisible(true);
						this.nextButton.setDisable(false);
						System.out.println("Connection successful.");
					}
					catch (Exception exp)
					{
						this.connectionOutcome.setFill(Color.RED);
						this.connectionOutcome.setText("Couldnt connect. Please check login details and try again.");
						this.connectionOutcome.setVisible(true);
						this.nextButton.setDisable(true);
					}
				}
				else if (source.getId().equals(nextButton.getId()))
				{
					this.task.cancel();
					//this.thread = new Thread(task);
					try
					{
						if (this.thread != null)
							this.thread.join();
					}
					catch (Exception ex)
					{
						System.out.println(ex);
					}

					if (super.nextView != null)
					{
						this.pane.getChildren().clear();

						//super.nextView.start(this.stage);
						List<String> allWritableColumns = new LinkedList<>(this.loginCredentialsTableColumns);
						Set<String> primaryKeyColumns = ((ISQLParser) this.controller).getAutoIncrementColumnNames(this.loginCredentialsTableName);
						allWritableColumns.removeAll(primaryKeyColumns);

						//config-set list loginCredentialsTableColumns is ordered, and the order is important here because we add to it assuming column positions
						this.loginCredentialsTableData = new LinkedList<>();
						//Add data in the order "username", "password", "filename", "hostname"
						this.loginCredentialsTableData.add(this.userTextField.getText());
						this.loginCredentialsTableData.add(this.pwTextField.getText());
						this.loginCredentialsTableData.add(this.fnameTextField.getText());
						this.loginCredentialsTableData.add(this.hostTextField.getText());

						this.controller.write(this, this.loginCredentialsTableName, allWritableColumns, this.loginCredentialsTableData);
						System.out.println("Started view2");
					}
					else
					{
						System.out.println("View1: No next view set.");
					}
				}

				System.out.println("Is a button, get the text:" + ((Button) source).getText() + "sourceId:" + source.getId() + "btnId:" + this.testButton.getId());
			}
			else if (source.getId().equals(this.hostTextField.getId()) || (source.getId().equals(this.fnameTextField.getId())) || (source.getId().equals(this.userTextField.getId())) || (source.getId().equals(this.pwTextField.getId())))
			{
				this.connectionOutcome.setVisible(false);
			}
			else if (source instanceof ComboBox)
			{
				if (((ComboBox) source).getValue() == "Relational Database")
				{
					this.pane.getChildren().addAll(hostLabel, hostTextField, fnameLabel, fnameTextField, userLabel, userTextField, pwLabel, pwTextField, testButton, this.connectionOutcome);
					thread = new Thread(this.task);
					this.thread.start();
				}
				else
				{
					this.pane.getChildren().removeAll(hostLabel, hostTextField, fnameLabel, fnameTextField, userLabel, userTextField, pwLabel, pwTextField, testButton, this.connectionOutcome);
				}

				System.out.println("Is a combo, get the text:" + comboBox.getValue());
			}
			else
			{
				System.out.println("Error- unhandle event in class View1");
			}
		}
		catch (Exception except)
		{
			System.out.println("cView1: Exception in handle():" + except);
		}

	}

	public void setNextView(IDataTrainView nextView)
	{
		super.nextView = nextView;
	}

	/*
	@Override
	public void onApplicationWrite(String tableName)
	{

	}
	 */
	@Override
	public void onApplicationWrite(String table, Map<String, List<String>> data)
	{

	}

}
