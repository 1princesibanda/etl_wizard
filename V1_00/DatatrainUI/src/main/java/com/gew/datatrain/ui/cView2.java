/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gew.datatrain.ui;

import com.gew.datatrain.Controller.mysqlcontroller.IApplication;
import com.gew.datatrain.Controller.mysqlcontroller.IController;
import com.gew.datatrain.Controller.mysqlcontroller.cMySQLController;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Remember to use a controller object to write data out of the UI, and to use a non-blocking call if called from main app thread, otherwise call from a worker thread
 *
 * @author 0400626
 */
public class cView2 extends IDataTrainView implements EventHandler<Event>, IApplication
{

	private static final String DEFAULT_TABLE_NAME = "tblQueries", DEFAULT_COLUMN_NAME = "querySql", DEFAULT_TABLE_INFORMATION_TABLE_NAME = "INFORMATION_SCHEMA.PARTITIONS", DEFAULT_COLUMN_INFORMATION_TABLE_NAME = "INFORMATION_SCHEMA.columns", DEFAULT_MAP_TABLE_NAME = "tblMaps", DEFAULT_MAPS_TABLE_MAP_NAME_COLUMN_NAME = "mapName";
	private static final String DEFAULT_LOGIN_CREDENTIALS_TABLENAME = "loginCredentials";

	private static final List<String> DEFAULT_TABLE_INFORMATION_TABLE_COLUMNS_LIST = Arrays.asList("TABLE_NAME"), DEFAULT_COLUMN_INFORMATION_TABLE_COLUMNS_LIST = Arrays.asList("COLUMN_NAME");
	private static final List<String> DEFAULT_LOGIN_CREDENTIALS_TABLE_COLUMNS = Arrays.asList("loginCredentialsId", "username", "password", "filename", "hostname");
	private static final List<String> DEFAULT_MAP_NAMES_TABLE_COLUMNS_LIST = Arrays.asList("mapName");
	private String queryStringTableName, queryStringTargetColumnName, qualifiedUserQuery, loginCredentialsTableName;
	private String tableInformationTableName, columnInformationTableName, mapNamesTableName, mapsTableMapNameColumnName;
	private boolean INITIALISED;
	private Map<String, List<String>> readOut;
	Pane pane;
	Scene scene;
	IController controller;
	Bounds bounds;
	Text scenetitle, userQueryTestOutcome;
	Label mapNamesLabel, tableNamesLabel, tableColumnsLabel, sqlQueryLabel, mapNameTextAreaLabel;
	ListView<String> tableNamesListView, columnNamesListView, mapsListView;
	ObservableList<String> tableNamesList, columnNamesList, mapNamesList;
	List<String> tableList, columnList, tableInformationTableColumnsList, columnInformationTableColumnsList, loginCredentialsTableColumns, mapNamesTableColumnsList;
	TextArea queryTextArea, mapNameTextArea;
	Button loadMapButton, deleteMapButton, nextButton, testQueryButton, explainButton, backButton;

	Task<Void> task;
	Thread thread;

	public cView2(Pane pane)
	{
		this.pane = pane;
		this.scene = new Scene(this.pane, 200, 200);
	}

	public cView2()
	{
		this.INITIALISED = false;
		this.pane = new Pane();
		this.scene = new Scene(this.pane, 1600, 500);

		this.scenetitle = new Text("Data selection SQL");
		this.userQueryTestOutcome = new Text();

		this.mapNamesLabel = new Label("MAP NAMES");
		this.tableNamesLabel = new Label("TABLE NAMES");
		this.tableColumnsLabel = new Label("COLUMN NAMES");
		this.sqlQueryLabel = new Label("Oracle SQL query:");
		this.mapNameTextAreaLabel = new Label("Enter New Map Name:");

		this.tableNamesListView = new ListView<>();
		this.columnNamesListView = new ListView<>();
		this.mapsListView = new ListView<>();

		this.tableList = new LinkedList<>();
		this.columnList = new LinkedList<>();
		this.mapNamesTableColumnsList = new LinkedList<>();

		this.queryTextArea = new TextArea();
		this.mapNameTextArea = new TextArea();

		this.nextButton = new Button("Next");
		this.backButton = new Button("Back");
		this.testQueryButton = new Button("Test Query");
		this.explainButton = new Button("Explain");
		this.loadMapButton = new Button("LoadMap");
		this.deleteMapButton = new Button("DeleteMap");

		this.controller = null;

		this.queryStringTableName = null;
		this.qualifiedUserQuery = null;
		this.loginCredentialsTableName = null;
//
		this.task = new Task<Void>()
		{
			@Override
			protected Void call() throws Exception
			{
				while (!isCancelled())
				{
					if ((mapNamesList.contains(mapNameTextArea.getText())) || (mapNameTextArea.getText().equals("")))
					{
						mapsListView.setDisable(true);
						queryTextArea.setDisable(true);
						columnNamesListView.setDisable(true);
						tableNamesListView.setDisable(true);
						mapNameTextAreaLabel.setDisable(true);
						sqlQueryLabel.setDisable(true);
						tableColumnsLabel.setDisable(true);
						tableNamesLabel.setDisable(true);
						mapNamesLabel.setDisable(true);
					}
					else
					{
						mapsListView.setDisable(false);
						queryTextArea.setDisable(false);
						columnNamesListView.setDisable(false);
						tableNamesListView.setDisable(false);
						mapNameTextAreaLabel.setDisable(false);
						sqlQueryLabel.setDisable(false);
						tableColumnsLabel.setDisable(false);
						tableNamesLabel.setDisable(false);
						mapNamesLabel.setDisable(false);
					}
					mapNameTextArea.setDisable(false); //this widget always active
					Thread.sleep(100); //arguments are milliseconds 
				}
				return null;
			}
		};

		this.tableInformationTableName = null;
		this.columnInformationTableName = null;
		this.loginCredentialsTableColumns = null;
		this.tableInformationTableColumnsList = null;
		this.columnInformationTableColumnsList = null;
		this.mapNamesTableName = null;
		this.mapsTableMapNameColumnName = null;

		this.readOut = null;

		super.nextView = null;
		this.previousView = null;

	}

	public cView2(IController controller)
	{
		this();
		this.controller = controller;
	}

	public cView2(IController controller, IDataTrainView previousView, IDataTrainView nextView)
	{
		this();
		this.controller = controller;
		this.previousView = previousView;
		super.nextView = nextView;
		System.out.println("View2 Setting controller...");
	}

	@Override
	public void init()
	{
		this.INITIALISED = true;

		this.scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 20));
		this.scenetitle.setId(IDataTrainView.uniqueId());
		this.scenetitle.setX(700);
		this.scenetitle.setY(50);

		this.userQueryTestOutcome.setFont(Font.font("Tahoma", FontWeight.NORMAL, FontPosture.REGULAR, 10));
		this.userQueryTestOutcome.setId(IDataTrainView.uniqueId());
		this.userQueryTestOutcome.setX(800);
		this.userQueryTestOutcome.setY(100);

		this.mapNamesList = FXCollections.observableArrayList();
		this.tableNamesList = FXCollections.observableArrayList();
		this.columnNamesList = FXCollections.observableArrayList();

		bounds = this.mapNamesLabel.getLayoutBounds();
		this.mapNamesLabel.setDisable(true);
		Region.positionInArea(this.mapNamesLabel, 100, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNamesLabel.setId(IDataTrainView.uniqueId());

		bounds = this.tableNamesLabel.getLayoutBounds();
		this.tableNamesLabel.setDisable(true);
		Region.positionInArea(this.tableNamesLabel, 400, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.tableNamesLabel.setId(IDataTrainView.uniqueId());

		bounds = this.tableColumnsLabel.getLayoutBounds();
		this.tableColumnsLabel.setDisable(true);
		Region.positionInArea(this.tableColumnsLabel, 700, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.tableColumnsLabel.setId(IDataTrainView.uniqueId());

		bounds = this.sqlQueryLabel.getLayoutBounds();
		this.sqlQueryLabel.setDisable(true);
		Region.positionInArea(this.sqlQueryLabel, 1000, 130, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.sqlQueryLabel.setId(IDataTrainView.uniqueId());

		bounds = this.mapNameTextAreaLabel.getLayoutBounds();
		this.mapNameTextAreaLabel.setDisable(false);
		Region.positionInArea(this.mapNameTextAreaLabel, 100, 80, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNameTextAreaLabel.setId(IDataTrainView.uniqueId());

		this.mapsListView.setItems(this.mapNamesList);
		this.mapsListView.setDisable(true);
		this.bounds = this.mapsListView.getLayoutBounds();
		this.mapsListView.setPrefSize(240, 300);
		this.mapsListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.mapsListView, 100, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapsListView.setDisable(true);
		this.mapsListView.setOnMouseClicked(this);

		this.tableNamesListView.setItems(this.tableNamesList);
		this.tableNamesListView.setDisable(true);
		this.bounds = this.tableNamesListView.getLayoutBounds();
		tableNamesListView.setPrefSize(240, 300);
		this.tableNamesListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.tableNamesListView, 400, 160, 100, 100, 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.tableNamesListView.setOnMouseClicked(this);

		this.columnNamesListView.setItems(this.columnNamesList);
		this.columnNamesListView.setDisable(true);
		this.bounds = this.columnNamesListView.getLayoutBounds();
		columnNamesListView.setPrefSize(240, 300);
		this.columnNamesListView.setId(IDataTrainView.uniqueId());
		Region.positionInArea(this.columnNamesListView, 700, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.columnNamesListView.setOnMouseClicked(this);

		this.bounds = this.mapNameTextArea.getLayoutBounds();
		Region.positionInArea(this.mapNameTextArea, 240, 70, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.mapNameTextArea.setPrefSize(200, 10);
		this.mapNameTextArea.setId(IDataTrainView.uniqueId());
		this.mapNameTextArea.setOnKeyTyped(this);
		this.mapNameTextArea.setDisable(false);
		this.mapNameTextArea.setPromptText("Enter map name here."); //TextFormatter t = new TextFormatter(); //mapNameTextArea.setTextFormatter(scenetitle); Text

		this.bounds = this.queryTextArea.getLayoutBounds();
		this.queryTextArea.setDisable(true);
		Region.positionInArea(this.queryTextArea, 1000, 160, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.queryTextArea.setId(IDataTrainView.uniqueId());
		this.queryTextArea.setOnKeyTyped(this);
		queryTextArea.setPromptText("Enter Oracle Query here and test query with the test button");

		this.nextButton.setDisable(true);
		this.nextButton.setId(IDataTrainView.uniqueId());
		this.backButton.setDisable(true);
		this.backButton.setId(IDataTrainView.uniqueId());
		this.testQueryButton.setDisable(true);
		this.testQueryButton.setId(IDataTrainView.uniqueId());
		this.explainButton.setDisable(true);
		this.explainButton.setId(IDataTrainView.uniqueId());
		this.loadMapButton.setDisable(true);
		this.loadMapButton.setId(IDataTrainView.uniqueId());
		this.deleteMapButton.setDisable(true);
		this.deleteMapButton.setId(IDataTrainView.uniqueId());

		bounds = this.nextButton.getLayoutBounds();
		Region.positionInArea(this.nextButton, 1400, 410, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.nextButton.setOnMouseClicked(this);
		this.nextButton.setId(cView1.uniqueId());

		bounds = this.backButton.getLayoutBounds();
		Region.positionInArea(this.backButton, 1300, 410, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.backButton.setOnMouseClicked(this);

		bounds = this.testQueryButton.getLayoutBounds();
		Region.positionInArea(this.testQueryButton, 1000, 350, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.testQueryButton.setOnMouseClicked(this);

		bounds = this.explainButton.getLayoutBounds();
		Region.positionInArea(this.explainButton, 1100, 350, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.explainButton.setOnMouseClicked(this);
		this.explainButton.setId(cView1.uniqueId());

		bounds = this.loadMapButton.getLayoutBounds();
		Region.positionInArea(this.loadMapButton, 1000, 410, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.loadMapButton.setOnMouseClicked(this);
		this.loadMapButton.setId(cView1.uniqueId());

		bounds = this.deleteMapButton.getLayoutBounds();
		Region.positionInArea(this.deleteMapButton, 1100, 410, this.bounds.getWidth(), this.bounds.getHeight(), 0, Insets.EMPTY, HPos.LEFT, VPos.BASELINE, true);
		this.deleteMapButton.setOnMouseClicked(this);
		this.deleteMapButton.setId(cView1.uniqueId());

		this.queryStringTableName = this.queryStringTableName == null ? cView2.DEFAULT_TABLE_NAME : this.queryStringTableName;
		this.queryStringTargetColumnName = this.queryStringTargetColumnName == null ? cView2.DEFAULT_COLUMN_NAME : this.queryStringTargetColumnName;
		this.tableInformationTableName = this.tableInformationTableName == null ? cView2.DEFAULT_TABLE_INFORMATION_TABLE_NAME : this.tableInformationTableName;
		this.columnInformationTableName = this.columnInformationTableName == null ? cView2.DEFAULT_COLUMN_INFORMATION_TABLE_NAME : this.columnInformationTableName;
		this.tableInformationTableColumnsList = this.tableInformationTableColumnsList == null ? cView2.DEFAULT_TABLE_INFORMATION_TABLE_COLUMNS_LIST : this.tableInformationTableColumnsList;
		this.columnInformationTableColumnsList = this.columnInformationTableColumnsList == null ? cView2.DEFAULT_COLUMN_INFORMATION_TABLE_COLUMNS_LIST : this.columnInformationTableColumnsList;
		this.loginCredentialsTableColumns = this.loginCredentialsTableColumns == null ? cView2.DEFAULT_LOGIN_CREDENTIALS_TABLE_COLUMNS : this.loginCredentialsTableColumns;
		this.loginCredentialsTableName = this.loginCredentialsTableName == null ? cView2.DEFAULT_LOGIN_CREDENTIALS_TABLENAME : this.loginCredentialsTableName;
		this.mapNamesTableName = this.mapNamesTableName == null ? cView2.DEFAULT_MAP_TABLE_NAME : this.mapNamesTableName;
		this.mapNamesTableColumnsList = this.mapNamesTableColumnsList == null ? cView2.DEFAULT_MAP_NAMES_TABLE_COLUMNS_LIST : this.mapNamesTableColumnsList;
		this.mapsTableMapNameColumnName = this.mapsTableMapNameColumnName == null ? cView2.DEFAULT_MAPS_TABLE_MAP_NAME_COLUMN_NAME : this.mapsTableMapNameColumnName;

		this.thread = null;

		//super.applicationsSet.add(this);
		if (this.controller != null)
			this.controller.getApplications().add(this);
		//this.controller.setApplications(super.applicationsSet);
	}

	//make new scene using the pane
	@Override
	public void start(Stage stage)
	{
		if (!this.INITIALISED)
			this.init();
		//configure the stage
		//populateTableNames();
		stage.setTitle("Text Data Format Converter");
		stage.setMaxHeight(500);
		stage.setMaxWidth(1600);
		stage.setMinHeight(500);
		stage.setMinWidth(1300);
		stage.setFullScreen(false);
		stage.setResizable(false); //Toggles availability of Windows minimise and maximise. if set to false and setMaximized() is set, then it is fixed to the setting of setMaximized()
		stage.setMaximized(false);
		//stage.toFront(); Not really data- these are meant for use at runtime
		stage.setIconified(false); //Puts the window down at the bottom task bar where the open programs are. Takes precedence over toBack() and toFront();
		stage.setFullScreenExitHint("This is exit hint");
		stage.setAlwaysOnTop(false);

		this.stage = stage;
		this.pane.getChildren().setAll(this.scenetitle, this.mapNamesLabel, this.tableColumnsLabel, this.tableNamesLabel, this.sqlQueryLabel, this.mapNameTextAreaLabel, this.tableNamesListView, this.columnNamesListView, this.mapsListView, this.queryTextArea, this.userQueryTestOutcome, this.mapNameTextArea, this.nextButton, this.backButton, this.testQueryButton, this.deleteMapButton, this.loadMapButton);

		stage.setScene(this.scene);
		stage.show();
		this.thread = new Thread(this.task);
		this.thread.start();
	}

	public void populateTableNames()
	{
		//Iterator<String> tablesIter = , columns;
		try
		{
			String str = "select " + this.controller.toStringColumns(this.tableInformationTableName, this.tableInformationTableColumnsList) + " from " + this.tableInformationTableName + " WHERE TABLE_SCHEMA = '" + this.controller.getDatabaseName() + "';";
			System.out.println(str);
			this.tableInformationTableColumnsList = new LinkedList<>();
			this.readOut = this.controller.read(str, null, null, null, this.tableInformationTableColumnsList);
			//if
			populateData(this.tableInformationTableColumnsList, this.tableList, this.tableNamesList);
			//System.exit(0);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
		//String sql = "select " + this.controller.toStringColumns(tableName, columnInformationTableColumnsList) + " from INFORMATION_SCHEMA.columns WHERE TABLE_SCHEMA = '" + this.controller.getDatabaseName() + "'";
		//populateData(this.tableInformationTableName, this.tableInformationTableColumnsList, this.tableList, this.tableNamesList);
	}

	public void populateData(List<String> columnsList, List<String> bufferList, ObservableList<String> targetList)
	{
		bufferList.clear();
		try
		{
			assert this.readOut.keySet().containsAll(columnsList);
			Iterator<String> iter = columnsList.iterator(), temp;
			String columnName;
			List<String> list;
			while (iter.hasNext())
			{
				columnName = iter.next();
				list = this.readOut.get(columnName);
				temp = list.iterator();
				while (temp.hasNext())
				{
					bufferList.add(temp.next());
				}
			}

			targetList.setAll(bufferList);
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}
	}

	public void populateData(String tableName, List<String> columnsList, List<String> bufferList, ObservableList<String> targetList)
	{
		bufferList.clear();
		try
		{
			Map<String, List<String>> read = this.controller.read(tableName, columnsList);
			assert read.keySet().containsAll(columnsList);
			Iterator<String> iter = columnsList.iterator(), temp;
			String columnName;
			List<String> list;
			while (iter.hasNext())
			{
				columnName = iter.next();
				list = read.get(columnName);
				temp = list.iterator();
				while (temp.hasNext())
				{
					bufferList.add(temp.next());
				}
			}

			targetList.setAll(bufferList);
		}
		catch (Exception ex)
		{
			System.out.println(ex);
		}
	}

	public void populateColumnNames(String tableName)
	{
		try
		{
			String sql = "select " + this.controller.toStringColumns(tableName, columnInformationTableColumnsList) + " from INFORMATION_SCHEMA.columns WHERE TABLE_SCHEMA = '" + this.controller.getDatabaseName() + "' and table_name = '" + tableName + "'";
			System.out.println(sql);
			this.columnInformationTableColumnsList = new LinkedList<>();
			this.readOut = this.controller.read(sql, null, null, null, this.columnInformationTableColumnsList);
			//if
			//populateData(tableName, this.columnInformationTableColumnsList, this.columnList, this.columnNamesList);
			populateData(this.columnInformationTableColumnsList, this.columnList, this.columnNamesList);
		}
		catch (Exception e)
		{
			System.out.println(e);
		}
	}

	//ActionEvent>, EventHandler<MouseEvent>
	@Override
	public void handle(Event e)
	{
		Node source = (Node) e.getSource(); //Should always be a Node on the Scene graph
		try
		{
			if (source.getId().equals(this.backButton.getId()))
			{
				this.task.cancel();

				try
				{
					if (this.thread != null)
					{
						this.thread.join();
					}
				}
				catch (Exception ex)
				{
					System.out.println(ex);
				}

				if (this.previousView != null)
				{
					this.pane.getChildren().clear();
					this.previousView.start(this.stage);
				}

			}
			else if (source.getId().equals(nextButton.getId()))
			{
				this.task.cancel();
				//this.thread = new Thread(task);
				try
				{
					if (this.thread != null)
					{
						this.thread.join();
					}
				}
				catch (Exception ex)
				{
					System.out.println(ex);
				}

				//String sql = "insert into " + this.queryStringTableName + " (" + this.queryStringTargetColumnName + ") values ('" + this.qualifiedUserQuery + "');";
				try
				{
					//this.controller.write(this, this.queryStringTableName, sql);

					List<String> columnList_ = new LinkedList<>();
					List<String> valueList = new LinkedList<>();

					columnList_.add(this.queryStringTargetColumnName);
					columnList_.add(this.mapsTableMapNameColumnName);
					valueList.add(this.qualifiedUserQuery);
					valueList.add(this.mapNameTextArea.getText());
					this.controller.write(this, this.queryStringTableName, columnList_, valueList);

					/*
					valueList.clear();
					valueList.add(this.mapNameTextArea.getText());	
					this.controller.write(this, this.mapNamesTableName, this.mapNamesTableColumnsList, valueList);
					System.out.println("View2: Written to table " + this.queryStringTableName);
					 */
				}
				catch (Exception exp)
				{
					System.out.println("View2: Error writing to table " + this.queryStringTableName + ":" + exp);
				}

				this.pane.getChildren().clear();

			}
			else if (source.getId().equals(this.testQueryButton.getId()))
			{
				try
				{
					boolean userQueryHasNoData = true;
					String sql = this.queryTextArea.getText();
					this.readOut = this.controller.read(sql, null, null, null, null);//warn if returned rs with no columns or rs has no rows- this is the case if user query is good but doesnt match any data
					Collection<List<String>> userQueryData = this.readOut.values();
					Iterator<List<String>> iter = userQueryData.iterator();
					while (iter.hasNext())
					{
						if (!iter.next().isEmpty())
							userQueryHasNoData = false;
					}
					if (userQueryHasNoData)
					{
						this.userQueryTestOutcome.setFill(Color.ORANGE);
						this.userQueryTestOutcome.setText("OK, but No data.");
						this.userQueryTestOutcome.setVisible(true);
					}
					else
					{
						this.userQueryTestOutcome.setFill(Color.GREEN);
						this.userQueryTestOutcome.setText("OK!");
						this.userQueryTestOutcome.setVisible(true);
					}

					this.qualifiedUserQuery = sql; //only if the query passes the test- ie returns some RS
					this.nextButton.setDisable(false);
					this.explainButton.setDisable(false);
				}
				catch (Exception exp)
				{
					this.userQueryTestOutcome.setFill(Color.RED);
					this.userQueryTestOutcome.setText("Bad Query: " + exp);
					this.userQueryTestOutcome.setVisible(true);
				}

			}
			else if (source.getId().equals(this.explainButton.getId()))
			{
				System.out.println("view 2 Is a button, get the text:" + ((Button) source).getText() + "sourceId:" + source.getId() + "btnId:" + explainButton.getId());
			}
			else if (source.getId().equals(this.tableNamesListView.getId()))
			{
				int selectedTableNameIndex = this.tableNamesListView.getSelectionModel().getSelectedIndex();
				if (selectedTableNameIndex < 0)
				{
					this.columnList.clear();
				}
				else
				{
					String selectedTableName = this.tableNamesList.get(selectedTableNameIndex);
					populateColumnNames(selectedTableName);
					//populateColumnNames();
				}
			}
			else if (source.getId().equals(this.columnNamesListView.getId()))
			{
				System.out.println(this.columnNamesListView.getSelectionModel().getSelectedIndex());
			}
			else if (source.getId().equals(this.queryTextArea.getId()))
			{
				if (this.queryTextArea.getText().length() > 0)
				{
					//this.nextButton.disabledProperty().getValue() == true
					this.testQueryButton.setDisable(false);
					this.nextButton.setDisable(true);
					this.explainButton.setDisable(true);
				}
				else
				{
					assert this.queryTextArea.getText().length() == 0;
					this.testQueryButton.setDisable(true);
					this.nextButton.setDisable(true);
					this.explainButton.setDisable(true);
				}
			}
			else if (source.getId().equals(this.mapsListView.getId()))
			{
				this.loadMapButton.setDisable(false);
				this.deleteMapButton.setDisable(false);
				int index = this.mapsListView.getSelectionModel().getSelectedIndex();
				if (index > 0)
				{
					String mapName = this.mapNamesList.get(index);
					System.out.println("Selected map:" + mapName);
				}
			}
			else if (source.getId().equals(this.loadMapButton.getId()))
			{
				System.out.println("***print join:" + ((cMySQLController) this.controller).innerJoin("tblMaps", "tblQueries", "mapName"));
			}
			else
			{
				System.out.println("Unexpected button.");
			}
		}
		catch (Exception except)
		{
			System.out.println("cView2: Exception in handle():" + except);
		}
	}

	/*
	public void onApplicationRead(String str, String ostr)
	{

	}
	 */
 /*
	@Override
	public void onApplicationWrite(String tableName)
	{

	}
	 */
	@Override
	public void onApplicationWrite(String table, Map<String, List<String>> data)
	{
		assert table != null : "cView2: onAppWrite(): controller should never call this method without a table name set.";
		assert data != null : "cView2: onAppWrite(): controller should never call this method without data set.";

		if (table.equals(this.loginCredentialsTableName))
		{

		}
		else
		{
			System.out.println("cView2: onAppWrite():Ignoring uninteresting table name '" + table + "'.");
		}
	}

	@Override
	public void onViewWrite(String table, Map<String, List<String>> data)
	{
		assert table != null : "cView2: onViewWrite(): controller should never call this method without a table name set.";
		assert data != null : "cView2: onViewWrite(): controller should never call this method without data set.";

		if (table.equals(this.loginCredentialsTableName))
		{
			assert this.stage != null : "cView2: onViewWrite(): if cView2 object used with controller, stage should be set on which cView2 renders widgets, etc.";
			System.out.println("*****calling start in 2");
			this.start(this.stage);
			System.out.println("*****called start in 2");
			populateTableNames();
			System.out.println("*****called populate in 2");

		}
		/*
		if (table.equals(this.mapNamesTableName))
		{
			List<String> dataColumn = data.get(this.mapsTableMapNameColumnName);
			this.mapNamesList.setAll(dataColumn);
		}
		 */
		if (table.equals(this.mapNamesTableName))
		{
			try
			{
				data = ((cMySQLController) this.controller).readValue(this.mapNamesTableName);
			}
			catch (Exception ex)
			{
				Logger.getLogger(cView2.class.getName()).log(Level.SEVERE, null, ex);
			}
			Set<String> mapNamesSet = new HashSet<>();
			List<String> list = data.get("mapName");
			mapNamesSet.addAll(list);
			this.mapNamesList.setAll(mapNamesSet);
			this.start(this.stage);
			populateTableNames();
		}
		else
		{
			System.out.println("cView2: onViewWrite():Ignoring uninteresting table name '" + table + "'.");
		}
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}
