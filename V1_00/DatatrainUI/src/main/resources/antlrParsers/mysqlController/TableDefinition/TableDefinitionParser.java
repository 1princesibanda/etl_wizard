// Generated from ..\..\prince\ETL_Wizard\V1_00\DatatrainUI\src\main\resources\antlrGrammars\mysqlController\TableDefinition.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TableDefinitionParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PRIMARY=1, KEY=2, CREATE=3, ANYTEXT=4, COLON=5, COMMA=6, CLOSINGROUNDBRACE=7, 
		OPENINGROUNDBRACE=8, TINYINT=9, SMALLINT=10, YEAR=11, INT_=12, BIGINT=13, 
		UNSIGNED=14, FLOAT=15, DOUBLE=16, NUMERIC=17, DECIMAL=18, DATETIME=19, 
		TIMESTAMP=20, TIME=21, DATE=22, ID=23, INT=24, NEWLINE=25, WS=26;
	public static final int
		RULE_prog = 0, RULE_tableDefinition = 1, RULE_columnNameList = 2, RULE_columnDefinition = 3, 
		RULE_columnDefinitionList = 4, RULE_sqlDataType = 5, RULE_directType = 6, 
		RULE_precisionType = 7, RULE_tableName = 8;
	private static String[] makeRuleNames() {
		return new String[] {
			"prog", "tableDefinition", "columnNameList", "columnDefinition", "columnDefinitionList", 
			"sqlDataType", "directType", "precisionType", "tableName"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'PRIMARY'", "'KEY'", "'CREATE'", null, "';'", "','", "')'", "'('", 
			"'TINYINT'", "'SMALLINT'", "'YEAR'", "'INT'", "'BIGINT'", "'UNSIGNED'", 
			"'FLOAT'", "'DOUBLE'", "'NUMERIC'", "'DECIMAL'", "'DATETIME'", "'TIMESTAMP'", 
			"'TIME'", "'DATE'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "PRIMARY", "KEY", "CREATE", "ANYTEXT", "COLON", "COMMA", "CLOSINGROUNDBRACE", 
			"OPENINGROUNDBRACE", "TINYINT", "SMALLINT", "YEAR", "INT_", "BIGINT", 
			"UNSIGNED", "FLOAT", "DOUBLE", "NUMERIC", "DECIMAL", "DATETIME", "TIMESTAMP", 
			"TIME", "DATE", "ID", "INT", "NEWLINE", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "TableDefinition.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TableDefinitionParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class ProgContext extends ParserRuleContext {
		public List<TableDefinitionContext> tableDefinition() {
			return getRuleContexts(TableDefinitionContext.class);
		}
		public TableDefinitionContext tableDefinition(int i) {
			return getRuleContext(TableDefinitionContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitProg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(19); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(18);
				tableDefinition();
				}
				}
				setState(21); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CREATE );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableDefinitionContext extends ParserRuleContext {
		public TerminalNode CREATE() { return getToken(TableDefinitionParser.CREATE, 0); }
		public TerminalNode ANYTEXT() { return getToken(TableDefinitionParser.ANYTEXT, 0); }
		public TableNameContext tableName() {
			return getRuleContext(TableNameContext.class,0);
		}
		public TerminalNode PRIMARY() { return getToken(TableDefinitionParser.PRIMARY, 0); }
		public TerminalNode KEY() { return getToken(TableDefinitionParser.KEY, 0); }
		public TerminalNode COLON() { return getToken(TableDefinitionParser.COLON, 0); }
		public ColumnDefinitionListContext columnDefinitionList() {
			return getRuleContext(ColumnDefinitionListContext.class,0);
		}
		public ColumnNameListContext columnNameList() {
			return getRuleContext(ColumnNameListContext.class,0);
		}
		public TableDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableDefinition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitTableDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableDefinitionContext tableDefinition() throws RecognitionException {
		TableDefinitionContext _localctx = new TableDefinitionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_tableDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23);
			match(CREATE);
			setState(24);
			match(ANYTEXT);
			setState(25);
			tableName();
			{
			setState(26);
			columnDefinitionList();
			}
			setState(27);
			match(PRIMARY);
			setState(28);
			match(KEY);
			{
			setState(29);
			columnNameList();
			}
			setState(30);
			match(COLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnNameListContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public TerminalNode COMMA() { return getToken(TableDefinitionParser.COMMA, 0); }
		public ColumnNameListContext columnNameList() {
			return getRuleContext(ColumnNameListContext.class,0);
		}
		public ColumnNameListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnNameList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitColumnNameList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnNameListContext columnNameList() throws RecognitionException {
		ColumnNameListContext _localctx = new ColumnNameListContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_columnNameList);
		try {
			setState(36);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(32);
				match(ID);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(33);
				match(ID);
				setState(34);
				match(COMMA);
				setState(35);
				columnNameList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public SqlDataTypeContext sqlDataType() {
			return getRuleContext(SqlDataTypeContext.class,0);
		}
		public TerminalNode ANYTEXT() { return getToken(TableDefinitionParser.ANYTEXT, 0); }
		public ColumnDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnDefinition; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitColumnDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnDefinitionContext columnDefinition() throws RecognitionException {
		ColumnDefinitionContext _localctx = new ColumnDefinitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_columnDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(38);
			match(ID);
			setState(39);
			sqlDataType();
			setState(40);
			match(ANYTEXT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColumnDefinitionListContext extends ParserRuleContext {
		public List<ColumnDefinitionContext> columnDefinition() {
			return getRuleContexts(ColumnDefinitionContext.class);
		}
		public ColumnDefinitionContext columnDefinition(int i) {
			return getRuleContext(ColumnDefinitionContext.class,i);
		}
		public TerminalNode COMMA() { return getToken(TableDefinitionParser.COMMA, 0); }
		public ColumnDefinitionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_columnDefinitionList; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitColumnDefinitionList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ColumnDefinitionListContext columnDefinitionList() throws RecognitionException {
		ColumnDefinitionListContext _localctx = new ColumnDefinitionListContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_columnDefinitionList);
		try {
			setState(47);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(42);
				columnDefinition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(43);
				columnDefinition();
				setState(44);
				match(COMMA);
				setState(45);
				columnDefinition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SqlDataTypeContext extends ParserRuleContext {
		public DirectTypeContext directType() {
			return getRuleContext(DirectTypeContext.class,0);
		}
		public PrecisionTypeContext precisionType() {
			return getRuleContext(PrecisionTypeContext.class,0);
		}
		public SqlDataTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sqlDataType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitSqlDataType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SqlDataTypeContext sqlDataType() throws RecognitionException {
		SqlDataTypeContext _localctx = new SqlDataTypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_sqlDataType);
		try {
			setState(51);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TINYINT:
			case SMALLINT:
			case YEAR:
			case INT_:
			case BIGINT:
			case UNSIGNED:
			case FLOAT:
			case DOUBLE:
			case NUMERIC:
			case DECIMAL:
			case DATETIME:
			case TIMESTAMP:
			case TIME:
			case DATE:
				enterOuterAlt(_localctx, 1);
				{
				setState(49);
				directType();
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 2);
				{
				setState(50);
				precisionType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DirectTypeContext extends ParserRuleContext {
		public TerminalNode TINYINT() { return getToken(TableDefinitionParser.TINYINT, 0); }
		public TerminalNode SMALLINT() { return getToken(TableDefinitionParser.SMALLINT, 0); }
		public TerminalNode YEAR() { return getToken(TableDefinitionParser.YEAR, 0); }
		public TerminalNode INT_() { return getToken(TableDefinitionParser.INT_, 0); }
		public TerminalNode BIGINT() { return getToken(TableDefinitionParser.BIGINT, 0); }
		public TerminalNode UNSIGNED() { return getToken(TableDefinitionParser.UNSIGNED, 0); }
		public TerminalNode FLOAT() { return getToken(TableDefinitionParser.FLOAT, 0); }
		public TerminalNode DOUBLE() { return getToken(TableDefinitionParser.DOUBLE, 0); }
		public TerminalNode NUMERIC() { return getToken(TableDefinitionParser.NUMERIC, 0); }
		public TerminalNode DECIMAL() { return getToken(TableDefinitionParser.DECIMAL, 0); }
		public TerminalNode DATETIME() { return getToken(TableDefinitionParser.DATETIME, 0); }
		public TerminalNode TIMESTAMP() { return getToken(TableDefinitionParser.TIMESTAMP, 0); }
		public TerminalNode TIME() { return getToken(TableDefinitionParser.TIME, 0); }
		public TerminalNode DATE() { return getToken(TableDefinitionParser.DATE, 0); }
		public DirectTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_directType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitDirectType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DirectTypeContext directType() throws RecognitionException {
		DirectTypeContext _localctx = new DirectTypeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_directType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TINYINT) | (1L << SMALLINT) | (1L << YEAR) | (1L << INT_) | (1L << BIGINT) | (1L << UNSIGNED) | (1L << FLOAT) | (1L << DOUBLE) | (1L << NUMERIC) | (1L << DECIMAL) | (1L << DATETIME) | (1L << TIMESTAMP) | (1L << TIME) | (1L << DATE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrecisionTypeContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public TerminalNode OPENINGROUNDBRACE() { return getToken(TableDefinitionParser.OPENINGROUNDBRACE, 0); }
		public TerminalNode INT() { return getToken(TableDefinitionParser.INT, 0); }
		public TerminalNode CLOSINGROUNDBRACE() { return getToken(TableDefinitionParser.CLOSINGROUNDBRACE, 0); }
		public PrecisionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precisionType; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitPrecisionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrecisionTypeContext precisionType() throws RecognitionException {
		PrecisionTypeContext _localctx = new PrecisionTypeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_precisionType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55);
			match(ID);
			setState(56);
			match(OPENINGROUNDBRACE);
			setState(57);
			match(INT);
			setState(58);
			match(CLOSINGROUNDBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(TableDefinitionParser.ID, 0); }
		public TableNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tableName; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof TableDefinitionVisitor ) return ((TableDefinitionVisitor<? extends T>)visitor).visitTableName(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TableNameContext tableName() throws RecognitionException {
		TableNameContext _localctx = new TableNameContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_tableName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\34A\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\6\2\26"+
		"\n\2\r\2\16\2\27\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\5"+
		"\4\'\n\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\5\6\62\n\6\3\7\3\7\5\7\66"+
		"\n\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\2\2\13\2\4\6\b\n\f\16\20"+
		"\22\2\3\3\2\13\30\2;\2\25\3\2\2\2\4\31\3\2\2\2\6&\3\2\2\2\b(\3\2\2\2\n"+
		"\61\3\2\2\2\f\65\3\2\2\2\16\67\3\2\2\2\209\3\2\2\2\22>\3\2\2\2\24\26\5"+
		"\4\3\2\25\24\3\2\2\2\26\27\3\2\2\2\27\25\3\2\2\2\27\30\3\2\2\2\30\3\3"+
		"\2\2\2\31\32\7\5\2\2\32\33\7\6\2\2\33\34\5\22\n\2\34\35\5\n\6\2\35\36"+
		"\7\3\2\2\36\37\7\4\2\2\37 \5\6\4\2 !\7\7\2\2!\5\3\2\2\2\"\'\7\31\2\2#"+
		"$\7\31\2\2$%\7\b\2\2%\'\5\6\4\2&\"\3\2\2\2&#\3\2\2\2\'\7\3\2\2\2()\7\31"+
		"\2\2)*\5\f\7\2*+\7\6\2\2+\t\3\2\2\2,\62\5\b\5\2-.\5\b\5\2./\7\b\2\2/\60"+
		"\5\b\5\2\60\62\3\2\2\2\61,\3\2\2\2\61-\3\2\2\2\62\13\3\2\2\2\63\66\5\16"+
		"\b\2\64\66\5\20\t\2\65\63\3\2\2\2\65\64\3\2\2\2\66\r\3\2\2\2\678\t\2\2"+
		"\28\17\3\2\2\29:\7\31\2\2:;\7\n\2\2;<\7\32\2\2<=\7\t\2\2=\21\3\2\2\2>"+
		"?\7\31\2\2?\23\3\2\2\2\6\27&\61\65";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}