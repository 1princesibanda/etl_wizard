/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  0400626
 * Created: 07 May 2019
 */

CREATE TABLE IF NOT EXISTS loginCredentials (
	loginCredentialsId int auto_increment,
    username varchar(255) not null,
    password varchar(255) not null,
    filename varchar(255) not null,
    hostname varchar(255) not null,
mapName varchar(255) not null,
    PRIMARY KEY (loginCredentialsId)
)  ENGINE=INNODB;

create table if not exists databaseConnectStatus
(statusId int auto_increment,
status boolean not null,
statusMessage varchar(255) default "Success",
mapName varchar(255) not null,
primary key (statusId)
)engine=InnoDB;

CREATE TABLE IF NOT EXISTS tblQueries (
	queryId int auto_increment not null,
    querySql varchar(255) not null,
mapName varchar(255) not null,
    PRIMARY KEY (queryId)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS tblProperties (
	propertyId int auto_increment not null,
    columnName varchar(255) not null,
    fileReference varchar(255) not null,
    type varchar(255) not null,
mapName varchar(255) not null,
    PRIMARY KEY (propertyId)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS tblPropertiesInput (
	propertyId int auto_increment not null,
    columnName varchar(255) not null,
    fileReference varchar(255) not null,
    type varchar(255) not null,
mapName varchar(255) not null,
    PRIMARY KEY (propertyId)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS tblTransformations (
	transformationId int auto_increment not null,
    columnName varchar(255) not null,
    transformation varchar(255) not null,
mapName varchar(255) not null,
    PRIMARY KEY (transformationId)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS tblMapsFilenames (
	filenameId int auto_increment not null,
    filename varchar(255) not null,
mapName varchar(255) not null,
    PRIMARY KEY (filenameId)
)  ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS tblMaps (
    mapId int not null,
    mapName varchar(255) not null ,
    columnName varchar(255) not null,
    fileReference varchar(255) not null,
    type varchar(255) not null,
    transformation varchar(255) not null,
    filename varchar(255) not null,
    PRIMARY KEY (mapId)
)  ENGINE=INNODB;

/*
CREATE TABLE IF NOT EXISTS tblMapNames (
	mapName varchar(255) not null,
    PRIMARY KEY (mapName)
)  ENGINE=INNODB;
*/